<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyRelationshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::table('detail_pelanggaran', function(Blueprint $table) {
            $table->foreign('id_catatan_pelanggaran')->references('id')->on('catatan_pelanggaran')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('catatan_pelanggaran', function(Blueprint $table) {
            $table->foreign('id_member')->references('id')->on('members')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('detail_member', function(Blueprint $table) {
            $table->foreign('id_member')->references('id')->on('members')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_pelanggaran', function(Blueprint $table) {
            $table->dropForeign('detail_pelanggaran_id_catatan_pelanggaran_foreign');
        });

        Schema::table('catatan_pelanggaran', function(Blueprint $table) {
            $table->dropForeign('catatan_pelanggaran_id_member_foreign');
        });

        Schema::table('detail_member', function(Blueprint $table) {
            $table->dropForeign('detail_member_id_member_foreign');
        });
    }
}
