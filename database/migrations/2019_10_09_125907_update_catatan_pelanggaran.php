<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCatatanPelanggaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('catatan_pelanggaran', function (Blueprint $table) {
            $table->string("jenis_pekerjaan")->nullable();
            $table->string("nomor")->after("id_member")->nullable();
            $table->string("jalan")->nullable();
            $table->string("kota")->nullable();
            $table->string("jenis_kendaraan")->nullable();
            $table->string("warna")->nullable();
            $table->string("nomor_registrasi")->nullable();
            $table->string("jenis_kelamin")->nullable();
            $table->string("peradilan_militer")->nullable();
            $table->string("sitaan")->nullable();
            $table->string("penyidik_nama")->nullable();
            $table->string("penyidik_pangkat_nrp")->nullable();
            $table->string("penyidik_jabatan")->nullable();
            $table->string("penyidik_kesatuan")->nullable();
            $table->string("disahkan_di")->nullable();
            $table->string("disahkan_tgl")->nullable();
            $table->string("disahkan_nama")->nullable();
            $table->string("disahkan_pangkat_nrp")->nullable();
            $table->string("disahkan_jabatan")->nullable();
            $table->string("disahkan_kesatuan")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
