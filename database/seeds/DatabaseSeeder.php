<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // $this->call([MembersTableSeeder::class]);

        $data = [
            0 => [
                'pasal' => '278',
                'name' => 'Mengemudikan Ranmor beroda empat atau lebih di jalan yang tidak dilengkapi dengan perlengkapan berupa ban cadangan, segitiga, dongkrak, pembuka roda dan peralatan PPPK.'
            ],
            1 => [
                'pasal' => '280',
                'name' => 'Mengemudikan Ranmor di jalan yang tidak dipasangi tanda Nomor Ranmor yang di tetapkan.'
            ],
            2 => [
                'pasal' => '281',
                'name' => 'Mengemudi Ranmor di jalan yang tidak memiliki SIM.'
            ],
            3 => [
                'pasal' => '283',
                'name' => 'Mengemudi Ranmor di jalan secara tidak wajar & melakukan kegiatan lain atau di pengaruhi oleh suatu keadaan yang mengakibatkan gangguan konsentrasi dalam mengemudi di jalan.'
            ],
            4 => [
                'pasal' => '284',
                'name' => 'Mengemudi Ranmor dengan tidak mengutamakan keselamatan Pejalan kaki atau bersepeda.'
            ],
            5 => [
                'pasal' => '285(1)',
                'name' => 'Mengemudikan sepeda motor di jalan yang tidak memenuhi persyaratan teknis dan layak jalan yang meliputi kaca spion, klakson, lampu utama, lampu rem, lampu petunjuk arah, alat pemantul cahaya, alat pengukur kecepatan, knalpot dan kedalaman alur ban.'
            ],
            6 => [
                'pasal' => '285(2)',
                'name' => 'Mengemudikan Ranmor beroda empat atau lebih di jalan tidak memenuhi persyaratan teknis yang meliputi kaca spion, klakson, lampu utama, lampu rem, lampu tanda batas dimensi badan kendaraan, lampu gandingan, lampu rem, lampu petunjuk arah, alat pemantul cahaya, alat pengukur kecepatan, kedalaman alur ban, kaca depan spakbor, bumper, penggandengan, penempelan atau penghapus kaca.'
            ],
            7 => [
                'pasal' => '287',
                'name' => 'Mengemudikan Ranmor di jalan yang melanggar aturan perintah atau larangan yang dinyatakan dengan Rambu lalu lintas / Marka jalan.'
            ],
            8 => [
                'pasal' => '288(1)',
                'name' => 'Mengemudikan Ranmor di jalan yang tidak dilengkapi dengan STNK atau STCKB yang ditetapkan.'
            ],
            9 => [
                'pasal' => '288(2)',
                'name' => 'Mengemudikan Ranmor di jalan yang tidak dapat menunjukan SIM yang sah.'
            ],
            10 => [
                'pasal' => '289',
                'name' => 'Mengemudikan Ranmor atau Penumpang yang duduk di samping Pengemudi yang tidak mengenakan sabuk keselamatan.'
            ],
            11 => [
                'pasal' => '291(1)',
                'name' => 'Mengemudikan Sepeda Motor tidak mengenakan helm Standar Nasional Indonesia.'
            ],
            12 => [
                'pasal' => '292(2)',
                'name' => 'Mengemudikan sepeda Motor yang membiarkan penumpangnya tidak menggunakan Helm.'
            ],
            13 => [
                'pasal' => '293(2)',
                'name' => 'Mengemudikan Sepeda Motor di jaaln tanpa menyalakan lampu utama pada siang hari.'
            ]
        ];

        foreach ($data as $key => $value) {
            DB::table('master_pelanggaran')->insert([
                'pasal' => $value['pasal'],
                'name' => $value['name'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        $dataAdmin= [
            0 => [
                'name' => 'Komandan',
                'email' => 'mailwibowo93@gmail.com',
                'role' => 'Special',
                'password' => 'admin12345',
            ],
            1 => [
                'name' => 'Member',
                'email' => 'admin.regular@admin.com',
                'role' => 'Regular',
                'password' => 'admin12345',
            ],
        ];

        foreach ($dataAdmin as $key => $value) {
            DB::table('admins')->insert([
                'name' => $value['name'],
                'email' => $value['email'],
                'role' => $value['role'],
                'password' => Hash::make($value['password']),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
