@extends('template')

@section('title')
Update Member
@endsection

@section('activeMember')
active
@endsection

@section('pageName')
Update Member
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center><h4><i class="icon fa fa-ban"></i> Error!</h4></center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-check"></i> Success!</h4></center>
            <center>{{ @$success ? @$success : session('alert') }} </center>
        </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-info"></i> Info!</h4></center>
            <center>{{ @$info ? @$info : session('info') }} </center>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
        <h3 class="box-title">Update Member</h3>
            <div style="float: right;">
                <a class="btn btn-success" href="{{route('view.member')}}"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        @foreach ($data as $key => $value)
        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal" action="{{route('handle.member.update.save')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="hidden" name="id" value="{{ $value->id }}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" value="{{ $value->name }}" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPangkat" class="col-sm-2 control-label">Pangkat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="pangkat" value="{{ $value->pangkat }}" id="pangkat"
                                placeholder="Password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNrp" class="col-sm-2 control-label">NRP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="nrp" id="nrp" value="{{ $value->nrp }}" placeholder="NRP" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputjabatan" class="col-sm-2 control-label">Jabatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="jabatan" value="{{ $value->jabatan }}" id="jabatan"
                                placeholder="Jabatan" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Kesatuan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="kesatuan" id="kesatuan"
                                placeholder="Kesatuan" value="{{ $value->kesatuan }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Tinggi</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="tinggi" id="tinggi"
                                placeholder="Tinggi" value="{{ @$value->tinggi }}" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Berat</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="berat" id="berat"
                                placeholder="Berat" value="{{ @$value->berat }}" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Tempat Lahir</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir"
                                placeholder="Tempat Lahir" value="{{ @$value->tempat_lahir }}" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir"
                                placeholder="Tanggal Lahir" value="{{ @date('d F Y', strtotime($value->tanggal_lahir)) }}" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Agama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="agama" id="agama"
                                placeholder="Agama" value="{{ @$value->agama }}" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Alamat Rumah</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="alamat_rumah" id="alamat_rumah"
                                placeholder="Alamat Rumah" required>{{ @$value->alamat_rumah }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Alamat Kesatuan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="alamat_kesatuan" id="alamat_kesatuan"
                                placeholder="Alamat Kesatuan" required>{{ @$value->alamat_kesatuan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputFoto" class="col-sm-2 control-label">Foto</label>
                        <center>
                            <div class="col-sm-10" style="padding-bottom:20px">
                            <input type="file" class="form-control" id="foto" name="foto" placeholder="Foto">
                            </div>
                            <div class="col-sm-12">
                                <img id="image-preview" src="{{ $value->foto }}" onerror="this.src='{{ asset('default/noimage.png')}}';" width="300"/>
                            </div>
                        </center>
                    </div>
                    @endforeach
                </div>
                <!-- /.box-body -->
                <div class="box-footer" style="float:right;">
                    <a href="{{route('view.member')}}" type="button" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection
@section('script')
    <script src="{{ asset('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $(function () {
            $('input[name=tanggal_lahir]').datepicker({
                format: 'dd MM yyyy',
                autoclose: true
            });
        })
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#image-value-input').attr('value', e.target.result);
                    $('#image-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#foto").change(function(){
            readURL(this);
        });
    </script>
@endsection
