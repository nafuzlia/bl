@extends('template')

@section('title')
Tambah Riwayat Hidup
@endsection

@section('activeMember')
active
@endsection

@section('pageName')
Tambah Riwayat Hidup
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center><h4><i class="icon fa fa-ban"></i> Error!</h4></center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-check"></i> Success!</h4></center>
            <center>{{ @$success ? @$success : session('alert') }} </center>
        </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-info"></i> Info!</h4></center>
            <center>{{ @$info ? @$info : session('info') }} </center>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
        <h3 class="box-title">Tambah Riwayat {{@$data['header']}}</h3>
            <div style="float: right;">
                <a class="btn btn-success" href="{{route('view.member.detail',@$data['id'])}}"> Kembali</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal" action="{{route('handle.member.detail.create.save')}}" method="POST">
                @csrf
                @method('POST')
                <input type="hidden" name="id" value="{{@$data['id']}}">
                <input type="hidden" name="tipe" value="{{@$data['tipe']}}">
                <div class="box-body">
                    <div class="form-group">
                        @if (@$data['tipe'] === "UMUM")
                            <label for="riwayat" class="col-sm-2 control-label">Pendidikan Umum</label>
                        @endif
                        @if (@$data['tipe'] === "MILITER")
                            <label for="riwayat" class="col-sm-2 control-label">Pendidikan Militer</label>
                        @endif
                        @if (@$data['tipe'] === "JABATAN")
                            <label for="riwayat" class="col-sm-2 control-label">Jabatan</label>
                        @endif
                        <div class="col-sm-10">
                        <input type="text" class="form-control" name="riwayat" id="riwayat" value="" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tahun" class="col-sm-2 control-label">Tahun</label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="tahun_mulai" value="" id="tahun"
                                placeholder="Mulai (Harus diisi)" required>
                        </div>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="tahun_sampai" value="" id="tahun"
                                placeholder="Sampai ( Form Ini Boleh Kosong, Kalau Tahun Mulai Sama)">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer" style="float:right;">
                    <a href="{{route('view.member.detail',@$data['id'])}}" type="button" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection
@section('script')
<script src="{{ asset('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script>
    $(function () {
        $('input[name=tahun_mulai]').datepicker({
            format: 'yyyy',
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
        $('input[name=tahun_sampai]').datepicker({
            format: 'yyyy',
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    })
</script>
@endsection
