@extends('template')

@section('title')
Dashboard Member
@endsection

@section('activeMember')
active
@endsection

@section('pageName')
Dashboard Member
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center><h4><i class="icon fa fa-ban"></i> Error!</h4></center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-check"></i> Success!</h4></center>
            <center>{{ @$success ? @$success : session('alert') }} </center>
        </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-info"></i> Info!</h4></center>
            <center>{{ @$info ? @$info : session('info') }} </center>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Daftar Member</h3>
            <div style="float: right;">
            @if ((boolean)Session::get('is_admin'))
                <a class="btn btn-success" href="{{ route('view.member.create')}}"><i class="fa fa-plus"></i> Add New Member</a>
            @endif
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-responsive table-hover table-striped display custome-table data-table" id="data-table" style="width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Foto</th>
                        <th>NRP</th>
                        <th>Name</th>
                        <th>Pangkat</th>
                        <th>Kesatuan</th>
                        @if ((boolean)Session::get('is_admin'))
                            <th style="text-align: center;">Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{ asset('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ asset('/bower_components/fastclick/lib/fastclick.js')}}"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $('.data-table').DataTable({
                autoWidth: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('view.member') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false},
                    {data: 'foto', name: 'foto'},
                    {data: 'nrp', name: 'nrp'},
                    {data: 'name', name: 'name'},
                    {data: 'pangkat', name: 'pangkat'},
                    {data: 'kesatuan', name: 'kesatuan'},
                    @if ((boolean)Session::get('is_admin'))
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    @endif
                ]
        })
    })
</script>
<script>
    function deleteConfirm(param) {
        Swal.fire({
            title: 'Apakah kamu ingin menghapus data ini?',
            text: "data ini akan dihapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!'
            }).then((result) => {
            if (result.value) {
                Swal.fire(
                'Berhasil Dihapus!',
                'Data Berhasil Dihapus.',
                'success'
                )
                window.location =  ''+ window.location.href+'/delete/'+param;
            }
        })
    }
</script>
@endsection
