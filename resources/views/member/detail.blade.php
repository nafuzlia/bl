@extends('template')

@section('title')
Detail Member
@endsection

@section('activeMember')
active
@endsection

@section('pageName')
Detail Member
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
        </center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
        </center>
        <center>{{ @$success ? @$success : session('alert') }} </center>
    </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-info"></i> Info!</h4>
        </center>
        <center>{{ @$info ? @$info : session('info') }} </center>
    </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Detail Anggota</h3>
            <div style="float: right;">
                <a class="btn btn-success" href="{{route('view.member')}}"><i class="fa fa-chevron-left"></i> Kembali</a>
                @if ((boolean)Session::get('is_admin'))
                    <a class="btn btn-danger" href="{{route('view.pelanggaran.create')}}"><i class="fa fa-plus"></i> Buat Catatan Pelanggaran</a>
                @endif
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal">
                @foreach ($data as $key => $value)
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" value="{{ @$value->name }}"
                                placeholder="Nama" disabled>
                            <input type="hidden" id="id_member" value="{{ @$value->id }}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPangkat" class="col-sm-2 control-label">Pangkat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="pangkat" value="{{ @$value->pangkat }}"
                                id="pangkat" placeholder="Password"  readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNrp" class="col-sm-2 control-label">NRP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="nrp" id="nrp" value="{{ @$value->nrp }}"
                                placeholder="NRP"  readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputjabatan" class="col-sm-2 control-label">Jabatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="jabatan" value="{{ @$value->jabatan }}"
                                id="jabatan" placeholder="Jabatan"  readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Kesatuan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="kesatuan" id="kesatuan" placeholder="Kesatuan"
                                value="{{ @$value->kesatuan }}"  readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Tinggi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tinggi" id="tinggi"
                                placeholder="Tinggi" value="{{ @$value->tinggi }}"  readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Berat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="berat" id="berat"
                                placeholder="Berat" value="{{ @$value->berat }}"  readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Tempat Lahir</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir"
                                placeholder="Tempat Lahir" value="{{ @$value->tempat_lahir }}"  readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir"
                                placeholder="Tanggal Lahir" value="{{ @date('d F Y', strtotime($value->tanggal_lahir)) }}"  readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Agama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="agama" id="agama"
                                placeholder="Agama" value="{{ @$value->agama }}"  readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Alamat Rumah</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="alamat_rumah" id="alamat_rumah"
                                placeholder="Alamat Rumah" readonly>{{ @$value->alamat_rumah }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKesatuan" class="col-sm-2 control-label">Alamat Kesatuan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="alamat_kesatuan" id="alamat_kesatuan"
                                placeholder="Alamat Kesatuan" readonly>{{ @$value->alamat_kesatuan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputFoto" class="col-sm-2 control-label">Foto</label>
                        <center>
                            <div class="col-sm-10">
                                <img id="image_detail" src="{{ @$value->foto }}" onerror="this.src='{{ asset('default/noimage.png')}}';" width="300" />
                            </div>
                        </center>
                    </div>
                    <div class="form-group">
                        <label for="inputFoto" class="col-sm-2 control-label"></label>
                        <center>
                            <div class="col-sm-10">
                                @if (@$value->foto)
                                    <button class="btn btn-success" onclick="SaveToDisk()" >Download Foto</button>
                                @else
                                    <button class="btn btn-success" onclick="SaveToDisk()" disabled>Download Foto</button>
                                @endif
                            </div>
                        </center>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer" style="float:right;">
                    <a href="{{route('view.member')}}" type="button" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
                </div>
                <!-- /.box-footer -->
            </form>
            @endforeach
        </div>
        <!-- /.box-body -->
    </div>
{{--
    <br>
    <h3 style="text-align: center; font-weight: bold;">Riwayat Pelanggaran Anggota</h3>
    <br>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Catatan Pelanggaran Lalu Lintas</h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-responsive table-hover table-striped display custome-table data-table-umum" id="data-table-pelanggaran" style="width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal Pelanggaran</th>
                        <th>Status</th>
                        @if ((boolean)Session::get('is_admin'))
                            <th style="text-align: center;">Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    --}}
    <br>
    <h3 style="text-align: center; font-weight: bold;">Riwayat Hidup</h3>
    <br>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Riwayat Pendidikan Umum</h3>
            <div style="float: right;">
                @if ((boolean)Session::get('is_admin'))
                    <a class="btn btn-success" href="{{route('view.member.detail.create', [Request::segment(3), "UMUM"])}}"><i class="fa fa-plus"></i> Tambah Pendidikan Umum</a>
                @endif
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-responsive table-hover table-striped display custome-table data-table-umum" id="data-table-umum" style="width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Riwayat</th>
                        <th>Tahun</th>
                        @if ((boolean)Session::get('is_admin'))
                            <th style="text-align: center;">Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Riwayat Pendidikan Militer</h3>
            <div style="float: right;">
                @if ((boolean)Session::get('is_admin'))
                    <a class="btn btn-success" href="{{route('view.member.detail.create', [Request::segment(3),"MILITER"])}}"><i class="fa fa-plus"></i> Tambah Pendidikan Militer</a>
                @endif
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-responsive table-hover table-striped display custome-table data-table-militer" id="data-table-militer" style="width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Riwayat</th>
                        <th>Tahun</th>
                        @if ((boolean)Session::get('is_admin'))
                            <th style="text-align: center;">Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Riwayat Jabatan</h3>
            <div style="float: right;">
                @if ((boolean)Session::get('is_admin'))
                    <a class="btn btn-success" href="{{route('view.member.detail.create', [Request::segment(3),"JABATAN"])}}"><i class="fa fa-plus"></i> Tambah Pendidikan Jabatan</a>
                @endif
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-responsive table-hover table-striped display custome-table data-table-jabatan" id="data-table-jabatan" style="width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Riwayat</th>
                        <th>Tahun</th>
                        @if ((boolean)Session::get('is_admin'))
                            <th style="text-align: center;">Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{ asset('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ asset('/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- Download-Js -->
<script src="{{asset('/package/download-js/download.js')}}"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        $('#data-table-pelanggaran').DataTable({
                autoWidth: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route("ajax.member.detail.pelanggaran",[ Request::segment(3) ]) }}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false},
                    {data: 'tanggal', name: 'tanggal'},
                    {data: 'status', name: 'status'},
                    @if ((boolean)Session::get('is_admin'))
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    @endif
                ]
        })
        $('#data-table-umum').DataTable({
                autoWidth: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route("ajax.member.detail",[ Request::segment(3), "UMUM"]) }}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false},
                    {data: 'riwayat', name: 'riwayat'},
                    {data: 'tahun', name: 'tahun'},
                    @if ((boolean)Session::get('is_admin'))
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    @endif
                ]
        })
        $('#data-table-militer').DataTable({
                autoWidth: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route("ajax.member.detail",[ Request::segment(3), "MILITER"]) }}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false},
                    {data: 'riwayat', name: 'riwayat'},
                    {data: 'tahun', name: 'tahun'},
                    @if ((boolean)Session::get('is_admin'))
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    @endif
                ]
        })
        $('#data-table-jabatan').DataTable({
                autoWidth: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route("ajax.member.detail",[ Request::segment(3), "JABATAN"]) }}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false},
                    {data: 'riwayat', name: 'riwayat'},
                    {data: 'tahun', name: 'tahun'},
                    @if ((boolean)Session::get('is_admin'))
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    @endif
                ]
        })
    })
    function SaveToDisk() {
        return download($('#image_detail').attr('src'), $('#name').val()+'_foto.jpg')
    }
</script>
<script>
    function deletePelanggaranConfirm(param, param2) {
        Swal.fire({
            title: 'Apakah kamu ingin menghapus data ini?',
            text: "data ini akan dihapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!'
            }).then((result) => {
            if (result.value) {
                Swal.fire(
                'Berhasil Dihapus!',
                'Data Berhasil Dihapus.',
                'success'
                )
                window.location =  ''+ window.location.origin+'/catatan/pelanggaran/delete/'+param+'/'+param2;
            }
        })
    }
</script>
<script>
    function deleteConfirm(param, param2) {
        Swal.fire({
            title: 'Apakah kamu ingin menghapus data ini?',
            text: "data ini akan dihapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!'
            }).then((result) => {
            if (result.value) {
                Swal.fire(
                'Berhasil Dihapus!',
                'Data Berhasil Dihapus.',
                'success'
                )
                window.location =  ''+ window.location.origin+'/member/detail/delete/'+param+'/'+param2;
            }
        })
    }
</script>
@endsection
