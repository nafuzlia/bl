<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{ @$data['nama_pengendara'] ? 'Catatan Pelanggaran'.@$data['nama_pengendara'] : 'Catatan Pelanggaran' }}</title>

    <style>
        @page { margin: 1cm; margin-bottom: 0.15cm; font-family: Arial, Helvetica, sans-serif;}

        * {font-size: 10px;}

        table.pasal tbody tr td,
        table.pasal thead tr th { text-align: center; padding: 3px; font-size: 10px; }

        .text-size { font-size: 10px; }
        .text-header { font-size: 10px !important; }
        .text-capital { text-transform: capitalize; }

        table.pasal tbody tr td.text-left { text-align: left; }
        table.pasal tbody tr td input[type="checkbox"] { padding: 0px; }
        table.pasal tbody tr td input[type="checkbox"] { font-size: 18px !important; margin: -10px 0px; }

        table.user-data tbody tr td,
        table.user-data thead tr td { font-size: 10px; padding: 0.25px; }

        table.disahkan tbody tr td,
        table.penyidik tbody tr td { padding: 0.25px; }

        div.catatan { margin-top: 30px; }
        div.catatan div,
        div.catatan ol li { font-size: 6px; white-space: nowrap; }
        div.catatan ol { margin-left: -30px; }
    </style>
</head>

@php
    function getDay($hari){

        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;

            case 'Mon':
                $hari_ini = "Senin";
            break;

            case 'Tue':
                $hari_ini = "Selasa";
            break;

            case 'Wed':
                $hari_ini = "Rabu";
            break;

            case 'Thu':
                $hari_ini = "Kamis";
            break;

            case 'Fri':
                $hari_ini = "Jumat";
            break;

            case 'Sat':
                $hari_ini = "Sabtu";
            break;

            default:
                $hari_ini = "Tidak di ketahui";
            break;
        }

        return $hari_ini;

    }

    function getMonth($month){

        switch($month){
            case '01':
                $hari_ini = "Januari";
            break;

            case '02':
                $hari_ini = "Februari";
            break;

            case '03':
                $hari_ini = "Maret";
            break;

            case '04':
                $hari_ini = "April";
            break;

            case '05':
                $hari_ini = "Mei";
            break;

            case '06':
                $hari_ini = "Juni";
            break;

            case '07':
                $hari_ini = "Juli";
            break;

            case '08':
                $hari_ini = "Agustus";
            break;

            case '09':
                $hari_ini = "September";
            break;

            case '10':
                $hari_ini = "Oktober";
            break;

            case '11':
                $hari_ini = "November";
            break;

            case '12':
                $hari_ini = "Desember";
            break;

            default:
                $hari_ini = "Tidak di ketahui";
            break;
        }

        return $hari_ini;
    }
@endphp

<body>
    <center>
        <div style=" z-index: -3; position: absolute; opacity: 0.4; display: block; margin-left: auto; margin-right: auto; width: 95vw; height: auto; top: 350px;">
            <img src="{{ asset('default/Logo.jpeg') }}" width="45%" height="50%" alt="">
        </div>
    </center>
    <div style="position: static;">
        <div style="width: 100%;">
            <div style="width: 100%; float: left;">
                <h6 style="margin: 0;">KOMANDO DAERAH MILITER XII/TANJUNGPURA</h6>
                <h6 style="margin: 0; padding-left: 80px; ">POLISI MILITER</h6>
                <div style="width: 32%; border-bottom: 1px solid black; margin-top: 10px; margin-bottom: 10px;"></div>
                <h6 style="text-align: left; padding-top: 0px; margin-bottom: 0px; margin-right: 0px; margin-left: 0px;">UNTUK KEADILAN</h6>
                <h6 style="text-align: left; margin: 0;">NOMOR : <b class="text-capital">{{ $data['catatan'][0]->nomor }}<b/></h6>
            </div>
            <table style="float: right; margin-right: 12px;">
                <tbody>
                    <tr style="margin: 0px; padding-bottom: 0px;">
                        <td style="margin: 0px; padding-bottom: 0px; margin-bottom: -10px; margin-top: -10px;">ANGKATAN DARAT</td>
                        <td>
                            <input type="checkbox" style="font-size: 18px !important; margin-bottom: -5px; margin-top: -5px;" {{ $data['catatan'][0]->jenis_pekerjaan == 1 ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <td style="margin: 0px; padding-bottom: 0px; margin-bottom: -10px;">ANGKATAN LAUT</td>
                        <td>
                            <input type="checkbox" style="font-size: 18px !important; margin-bottom: -5px;" {{ $data['catatan'][0]->jenis_pekerjaan == 2 ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <td style="margin: 0px; padding-bottom: 0px; margin-bottom: -10px;">ANGKATAN UDARA</td>
                        <td>
                            <input type="checkbox" style="font-size: 18px !important; margin-bottom: -5px;" {{ $data['catatan'][0]->jenis_pekerjaan == 3 ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <td style="margin: 0px; padding-bottom: 0px; margin-bottom: -10px;">PNS</td>
                        <td>
                            <input type="checkbox" style="font-size: 18px !important; margin-bottom: -5px;" {{ $data['catatan'][0]->jenis_pekerjaan == 4 ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <td style="margin: 0px; padding-bottom: 0px; margin-bottom: -10px;">SIPIL</td>
                        <td>
                            <input type="checkbox" style="font-size: 18px !important; margin-bottom: -5px;" {{ $data['catatan'][0]->jenis_pekerjaan == 5 ? 'checked' : '' }}>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div>
        <div style="width: 100%; margin-top: -58px;">
            <div style="text-align: center;">
                <h5 class="text-header">
                    <strong>BERITA ACARA PELANGGARAN LALU LINTAS TERTENTU</strong>
                </h5>
                <h5 class="text-header" style="margin-top: -15px;">
                    <strong>( BALANG LALIN )</strong>
                </h5>
            </div>
        </div>

        @if (@$data['member'])
        @foreach ($data['member'] as $membersKey => $valueMember)
        <div style="width: 100%; margin-top: -5px; border: 1px solid black; padding: 10px; margin-right: 0px; margin-left: 0px; ">
            <div class="text-size" style="text-align: justify;">
                <b class="text-size" style="font-weight: normal; padding-left: 30px;"> </b>
                Pada hari ini <b class="text-size text-capital">{{ getDay(date('D', strtotime($data['catatan'][0]->tgl))) }}</b>
                ,tanggal <b class="text-size text-capital">{{ date('d', strtotime($data['catatan'][0]->tgl)) }}</b>
                ,bulan <b class="text-size text-capital">{{ getMonth(date('m', strtotime($data['catatan'][0]->tgl))) }}</b>
                ,tahun <b class="text-size text-capital">{{ date('Y', strtotime($data['catatan'][0]->tgl)) }}</b>
                ,sekira pukul <b class="text-size text-capital">{{ date('H', strtotime($data['catatan'][0]->tgl)) }}</b> : <b>{{ date('i', strtotime($data['catatan'][0]->tgl)) }}</b>
                di jalan <b class="text-size text-capital">{{ $data['catatan'][0]->jalan }}</b>,
                kota <b class="text-size text-capital">{{ $data['catatan'][0]->kota }}</b> ,
                petugas yang bertanda tangan di bawah ini, berdasarkan undang-undang RI Nomor 31
                Tahun 1997 tentang Peradilan Militer dan Keputusan Panglima
                TNI Nomor Kep/650/VII/2011
                tentang Penyerahan Perkara Pelanggaran Lalu Lintas,
                telah Melakukan pemeriksaan terhadap
                sebuah kendaraan jenis <b class="text-size text-capital">{{ $data['catatan'][0]->jenis_kendaraan }}</b>,
                warna <b class="text-size text-capital">{{ $data['catatan'][0]->warna }}</b> , <br>
                Nomor Registrasi <b class="text-size text-capital">{{ $data['catatan'][0]->nomor_registrasi }}</b>.
                Yang dikemudikan oleh seorang <b class="text-size text-capital">{{ $data['catatan'][0]->jenis_kelamin }}</b> :
            </div>
            <div style="margin-top: 5px;">
                <table class="user-data">
                    <tbody>
                        <tr>
                            <td>Name </td>
                            <td>:</td>
                            <td class="text-capital" style="font-weight: bold;">{{ $valueMember->name }}</td>
                        </tr>
                        <tr>
                            <td>Pangkat/NRP </td>
                            <td> : </td>
                            <td class="text-capital" style="font-weight: bold;">{{ $valueMember->pangkat }} / {{ $valueMember->nrp }}</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan/Jabatan </td>
                            <td> : </td>
                            <td class="text-capital" style="font-weight: bold;">{{ $valueMember->jabatan }}</td>
                        </tr>
                        <tr>
                            <td>Kesatuan </td>
                            <td> : </td>
                            <td class="text-capital" style="font-weight: bold;">{{ $valueMember->kesatuan }}</td>
                        </tr>
                        <tr>
                            <td>Alamat </td>
                            <td> : </td>
                            <td class="text-capital" style="font-weight: bold;">{{ $valueMember->alamat_rumah }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: justify; margin-top: 5px;">
                <i style="font-style: normal; padding-left: 30px;"> </i>
                Yang diduga atau patut diduga telah melakukan pelanggaran
                lalu lintas yang diatur dalam UU Nomor 22 Tahun 2009
                tentang lalu lintas dan Angkutan jalan :
            </div>
        </div>

        @endforeach
        @endif

        <table class="pasal" style="width: 100%; border: 1px solid black; border-top: 0px; border-collapse: collapse;" border="1">
            <thead>
                <tr>
                    <th style="text-align: center;">Pasal Yang Dilanggar</th>
                    <th style="text-align: center;">Jenis Pelanggaran</th>
                    <th style="text-align: center;">(V)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td >278</td>
                    <td class="text-left">Mengemudikan Ranmor beroda empat atau lebih di jalan
                        yang
                        tidak
                        dilengkapi dengan perlengkapan
                        berupa ban cadangan, segitiga, dongkrak, pembuka roda dan peralatan PPPK.
                    </td>
                    <td>
                        <input type="checkbox" value="1" {{(in_array('1', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>280</td>
                    <td class="text-left">Mengemudikan Ranmor di jalan yang tidak dipasangi
                        tanda
                        Nomor Ranmor
                        yang di tetapkan</td>
                    <td>
                        <input type="checkbox" value="2" {{(in_array('2', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>281</td>
                    <td class="text-left">Mengemudi Ranmor di jalan yang tidak memiliki SIM.
                    </td>
                    <td>
                        <input type="checkbox" value="3" {{(in_array('3', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>283</td>
                    <td class="text-left">Mengemudi Ranmor di jalan secara tidak wajar &
                        melakukan
                        kegiatan lain
                        atau di pengaruhi oleh suatu
                        keadaan yang mengakibatkan gangguan konsentrasi dalam mengemudi di jalan.
                    </td>
                    <td>
                        <input type="checkbox" value="4" {{(in_array('4', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>284</td>
                    <td class="text-left">Mengemudi Ranmor dengan tidak mengutamakan
                        keselamatan
                        Pejalan kaki
                        atau bersepeda.</td>
                    <td>
                        <input type="checkbox" value="5" {{(in_array('5', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>285(1)</td>
                    <td class="text-left">Mengemudikan sepeda motor di jalan yang tidak
                        memenuhi
                        persyaratan
                        teknis dan layak jalan yang
                        meliputi kaca spion, klakson, lampu utama, lampu rem, lampu petunjuk arah,
                        alat
                        pemantul
                        cahaya,
                        alat pengukur kecepatan, knalpot dan kedalaman alur ban.</td>
                    <td>
                        <input type="checkbox" value="6" {{(in_array('6', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>285(2)</td>
                    <td class="text-left">Mengemudikan Ranmor beroda empat atau lebih di jalan
                        tidak
                        memenuhi
                        persyaratan teknis yang meliputi
                        kaca spion, klakson, lampu utama, lampu rem, lampu tanda batas dimensi badan
                        kendaraan,
                        lampu
                        gandingan, lampu rem, lampu petunjuk arah, alat pemantul cahaya, alat
                        pengukur
                        kecepatan,
                        kedalaman
                        alur ban, kaca depan spakbor, bumper, penggandengan, penempelan atau
                        penghapus kaca.
                    </td>
                    <td>
                        <input type="checkbox" value="7" {{(in_array('7', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>287</td>
                    <td class="text-left">Mengemudikan Ranmor di jalan yang melanggar aturan
                        perintah
                        atau
                        larangan yang dinyatakan dengan
                        Rambu lalu lintas / Marka jalan.</td>
                    <td>
                        <input type="checkbox" value="8" {{(in_array('8', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>288(1)</td>
                    <td class="text-left">Mengemudikan Ranmor di jalan yang tidak dilengkapi
                        dengan
                        STNK atau
                        STCKB yang ditetapkan.</td>
                    <td>
                        <input type="checkbox" value="9" {{(in_array('9', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>288(2)</td>
                    <td class="text-left">Mengemudikan Ranmor di jalan yang tidak dapat
                        menunjukan SIM
                        yang sah.
                    </td>
                    <td>
                        <input type="checkbox" value="10" {{(in_array('10', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>289</td>
                    <td class="text-left">Mengemudikan Ranmor atau Penumpang yang duduk di
                        samping
                        Pengemudi
                        yang tidak mengenakan sabuk
                        keselamatan.</td>
                    <td>
                        <input type="checkbox" value="11" {{(in_array('11', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>291(1)</td>
                    <td class="text-left">Mengemudikan Sepeda Motor tidak mengenakan helm
                        Standar
                        Nasional
                        Indonesia.</td>
                    <td>
                        <input type="checkbox" value="12" {{(in_array('12', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>292(2)</td>
                    <td class="text-left">Mengemudikan sepeda Motor yang membiarkan
                        penumpangnya tidak
                        menggunakan Helm. </td>
                    <td>
                        <input type="checkbox" value="13" {{(in_array('13', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>293(2)</td>
                    <td class="text-left">Mengemudikan Sepeda Motor di jaaln tanpa menyalakan
                        lampu
                        utama pada
                        siang hari.</td>
                    <td>
                        <input type="checkbox" value="14" {{(in_array('14', $data['id_data_master']) ? 'checked' : '')}}/>
                    </td>
                </tr>
                <tr>
                    <td>Lain-lain</td>
                    <td class="text-left" style="font-weight: bold;"><br><hr style="border:0.5px dotted black;width: 100%; margin: 0px"></td>
                    <td><input type="checkbox"/></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-top: 5px;">
        <b>Keterangan jenis pelanggaran</b>
        <div style="border-bottom: 1px solid black; background-color: rgba(white, 0);">
            <br/>
        </div>
    </div>
    <div style="text-align: justify; margin-top: 5px;">
        <b class="text-size" style="font-weight: normal; padding-left: 30px;"> </b>
        Atas pelanggaran yang telah dilakukannya, kepada Pelanggar telah
        dibuatkan Berita Acara Pelanggaran Lalu Lintas dan kepadanya
        telah diberitahukan untuk mengahadap Hakim pada
        Peradilan Militer <b class="text-capital">{{ $data['catatan'][0]->peradilan_militer }}</b>
        pada tanggal/waktu yang telah
        ditetapkan sesuai dengan surat panggilan.
    </div>
    <div style="text-align: justify; margin-top: 5px;">
        <b class="text-size" style="font-weight: normal; padding-left: 30px;"> </b>
        Untuk kepentingan penyidik selanjutnya/menghentikan
        pelanggaran yang telah terjadi, berdasarkan Pasal 260 UU No. 22 Tahun 2009,
        petugas menahan/menyita untuk sementara :
        KENDARAAN/SIM/STNK/BENDA LAIN
        berupa : <b class="text-capital">{{ $data['catatan'][0]->sitaan }}</b>
    </div>
    <div style="text-align: justify; margin-top: 5px;">
        <b class="text-size" style="font-weight: normal; padding-left: 30px;"> </b>
        Demikian Berita Acara ini dibuat dengan sebenarnya,
        kemudian untuk menguatkannya Tersangka,
        dan penyidik telah membubuhkan
        tanda tangan seperti tersebut dibawah ini :
    </div>
    <div style="width: 100%; margin-top: 5px;">
        <div style="width: 5%; float: left;"> </div>
        <div style="width: 37.5%; float: left;">
            <b><u>Tersangka</u></b>
            <div>Tanda Tangan</div>
        </div>
        <div style="width: 30%; float: left;">
            <b><u>Penyidik</u></b>
            <div>
                <table class="penyidik">
                    <tbody>
                        <tr>
                            <td>Nama</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->penyidik_nama }}</b></td>
                        </tr>
                        <tr>
                            <td>Pangkat/NRP</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->penyidik_pangkat_nrp }}</b></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->penyidik_jabatan }}</b></td>
                        </tr>
                        <tr>
                            <td>Kesatuan</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->penyidik_kesatuan }}</b></td>
                        </tr>
                        <tr>
                            <td>Tanda Tangan</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="width: 27.5%; float: left; padding-left: 20px;">
            <b><u>Disahkan</u></b>
            <div>
                <table class="disahkan">
                    <tbody>
                        <tr>
                            <td>Di</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->disahkan_di }}</b></td>
                        </tr>
                        <tr>
                            <td>Pada Tanggal</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->disahkan_tgl }}</b></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->disahkan_nama }}</b></td>
                        </tr>
                        <tr>
                            <td>Pangkat/NRP</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->disahkan_pangkat_nrp }}</b></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->disahkan_jabatan }}</b></td>
                        </tr>
                        <tr>
                            <td>Kesatuan</td>
                            <td> : </td>
                            <td><b class="text-capital">{{ $data['catatan'][0]->disahkan_kesatuan }}</b></td>
                        </tr>
                        <tr>
                            <td>Tanda Tangan</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="catatan" style="width: 30%; margin-top: 40px;">
        <div style="margin-bottom: -10px; margin-top: 30px;">Catatan :</div>
        <ol>
            <li>Terhadap pelanggaran yang terjadi, pada kolom yang tersedia diberikan tanda silang.</li>
            <li>Balang Lalin ini berlaku pula sebagai bukti penyitaan.</li>
            <li>Lembar Merah Untuk Pelanggar.</li>
            <li>Lembar Kuning Untuk Ankum.</li>
            <li>Lembar Hijau Untuk Dilmil/Dilmilti.</li>
            <li>Lembar Biru Untuk Otmil/Otmilti.</li>
            <li>Lembar Putih Untuk Arsip Kesatuan.</li>
        </ol>
    </div>
</body>
</html>
