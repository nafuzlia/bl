<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title') | Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css')}}">
    @yield('css')
    <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/skins/skin-blue.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/custom.css')}}">
    <style>
        .custome-table {
            overflow-x: scroll !important;
            text-align: center;
        }
        .custome-table thead tr th,
        .custome-table tbody tr td {
            max-width: 100px !important;
            text-overflow:ellipsis !important;
            overflow:hidden;
            white-space:nowrap;
        },
        .custome-table tbody tr {
            background-color: white !important;
        }
        .custome-table tbody tr:hover {
            background-color: rgba(102,102,102,0.2) !important;
        }

        #back2Top {
            width: 40px;
            line-height: 40px;
            overflow: hidden;
            z-index: 999;
            display: none;
            cursor: pointer;
            -moz-transform: rotate(270deg);
            -webkit-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg);
            position: fixed;
            bottom: 50px;
            right: 0;
            background-color: #3c8dbc;
            color: #FFFF;
            text-align: center;
            font-size: 30px;
            text-decoration: none;
        }

        #back2Top:hover {
            background-color: #333;
            color: #FFF;
        }
        .skin-blue .main-header li.user-header {
            background-color: #20623F !important;
        }
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0;
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="{{ route('view.dashboard')}}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>{{strtoupper(substr(str_replace(' ', '', (@(boolean)Session::get('is_admin') ? "Komandan" : "Member")), 0, 1))}}</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>{{(@(boolean)Session::get('is_admin') ? "Komandan" : "Member")}}</b></span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('default/default.png') }}" class="user-image" alt="User Image">
                                <span class="hidden-xs">{{@Session::get('name')}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ asset('default/default.png') }}" class="img-circle" alt="User Image">
                                    <p>
                                        {{(@(boolean)Session::get('is_admin') ? "Komandan" : "Member")}} <br>
                                        <span style="font-weight: lighter !important; font-size: 15px;">{{@Session::get('email')}}</span>
                                    </p>
                                    <br>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                        </div>
                                        <div class="col-xs-4 text-center">
                                        </div>
                                        <div class="col-xs-4 text-center">
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                    </div>
                                    <div class="pull-right">
                                    <a href="{{route('handle.logout')}}" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                </li>
                            </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src={{ asset('default/default.png') }} class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>
                            {{@Session::get('name')}}
                            <br>
                            <span style="font-weight: lighter !important; font-size: 10px; margin-top: 20px;">{{@Session::get('email')}}</span>
                        </p>
                    </div>
                </div>
                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN</li>
                    <!-- Optionally, you can add icons to the links -->
                    <li class="@yield('activeDashboard')"><a href="{{route('view.catatan.pelanggaran')}}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
                    @if ((boolean)Session::get('is_admin'))
                        <li class="@yield('activeMember')"><a href="{{route('view.member')}}"><i class="fa fa-users"></i><span>Manage Anggota</span></a></li>
                        <li class="header">Master Data</li>
                        <li class="@yield('activeMasterPelanggaran')"><a href="{{route('view.master.pelanggaran')}}"><i class="fa fa-book"></i> <span>Master Pelanggaran</span></a></li>
                        <li class="@yield('activeAdmin')"><a href="{{route('view.admin')}}"><i class="fa fa-user"></i><span>Manage Admin</span></a></li>
                    @else
                        <li class="@yield('activeMember')"><a href="{{route('view.member')}}"><i class="fa fa-users"></i><span>Daftar Anggota</span></a></li>
                        <li class="@yield('activeMasterPelanggaran')"><a href="{{route('view.master.pelanggaran')}}"><i class="fa fa-book"></i> <span>Daftar Jenis Pelanggaran</span></a></li>
                    @endif
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('namePage')
                </h1>
            </section>

            @yield('content')
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- Default to the left -->
            <strong>Copyright &copy; <?php echo date("Y");?>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="{{ asset('/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
    <!-- SweetAlert2 CDN-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.all.min.js"></script>
    @yield('script')

    <script>
        /*Scroll to top when arrow up clicked BEGIN*/
        $(window).scroll(function() {
            var height = $(window).scrollTop();
            if (height > 100) {
                $('#back2Top').fadeIn();
            } else {
                $('#back2Top').fadeOut();
            }
        });
        $(document).ready(function() {
            $("#back2Top").click(function(event) {
                event.preventDefault();
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            });

        });
        /*Scroll to top when arrow up clicked END*/
    </script>
</body>

</html>
