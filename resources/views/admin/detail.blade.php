@extends('template')

@section('title')
Detail Admin
@endsection

@section('activeAdmin')
active
@endsection

@section('pageName')
Detail Admin
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center><h4><i class="icon fa fa-ban"></i> Error!</h4></center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-check"></i> Success!</h4></center>
            <center>{{ @$success ? @$success : session('alert') }} </center>
        </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-info"></i> Info!</h4></center>
            <center>{{ @$info ? @$info : session('info') }} </center>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Detail Admin</h3>
            <div style="float: right;">
                <a class="btn btn-success" href="{{route('view.admin')}}"> Kembali</a>
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal">
                @foreach ($data as $key => $value)
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" value="{{ @$value->name }}" placeholder="Nama" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPangkat" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" value="{{ @$value->email }}" id="email"
                                placeholder="Email" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPangkat" class="col-sm-2 control-label">Role</label>
                        <div class="col-sm-10">

                            @if (@$value->role === 'Special')
                                <input type="text" class="form-control" value="Komandan" id="email"
                                placeholder="Role" readonly>
                            @endif

                            @if (@$value->role === 'Regular')
                                <input type="text" class="form-control" value="Member" id="email"
                                placeholder="Role" readonly>
                            @endif

                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- /.box-body -->
                <div class="box-footer" style="float:right;">
                    <a href="{{route('view.admin')}}" type="button" class="btn btn-primary">Kembali</a>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection
