<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Catatan Pelanggaran</title>
</head>
<body>
    <h3>PDF Catatan Pelanggaran Dari : </h3>

    @if (@$member)
    @foreach ($member as $membersKey => $valueMember)
    <table>
        <tbody>
            <tr>
                <td>Name </td>
                <td>:</td>
                <td style="font-weight: bold; text-transform: capitalize;">{{ @$valueMember->name }}</td>
            </tr>
            <tr>
                <td>Pangkat/NRP </td>
                <td> : </td>
                <td  style="font-weight: bold; text-transform: capitalize;">{{ @$valueMember->pangkat }} / {{ @$valueMember->nrp }}</td>
            </tr>
            <tr>
                <td>Pekerjaan/Jabatan </td>
                <td> : </td>
                <td style="font-weight: bold; text-transform: capitalize;">{{ @$valueMember->jabatan }}</td>
            </tr>
            <tr>
                <td>Kesatuan </td>
                <td> : </td>
                <td style="font-weight: bold; text-transform: capitalize;">{{ @$valueMember->kesatuan }}</td>
            </tr>
            <tr>
                <td>Alamat </td>
                <td> : </td>
                <td style="font-weight: bold; text-transform: capitalize;">{{ @$valueMember->alamat_rumah }}</td>
            </tr>
        </tbody>
    </table>
    @endforeach
    @endif

    <p>Untuk keterangan lebih lanjut silahkan lihat pdf yang terlampir di bawah ini. Sekian dan terima kasih!</p>
</body>
</html>
