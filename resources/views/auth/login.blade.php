<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/login.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/reset.min.css')}}">
</head>
<body>
    <div class="container">
        <div class="form-login">
            <div class="header">
                Rancangan Bangun <i style="font-style: italic;">Content Management System</i><br>
                Pelaporan pelanggaran Anggota TNI AD <br>
                Menggunakan Barcode Scanner
            </div>
            <div class="bg-alert">
                @if ($errors->any())
                <div class="alert alert-danger width-100-login">
                    <strong>Whoops!</strong>  Error!<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if (@$error || Session::has('alertErr'))
                <div class="alert alert-danger alert-dismissible width-100-login">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <center><h4><i class="icon fa fa-ban"></i> Error!</h4></center>
                    <center>{{ @$error ? @$error : session('alertErr') }} </center>
                </div>
                @endif

                @if (@$success || Session::has('alert'))
                <div class="alert alert-success alert-dismissible width-100-login">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <center><h4><i class="icon fa fa-check"></i> Success!</h4></center>
                        <center>{{ @$success ? @$success : session('alert') }} </center>
                    </div>
                @endif

                @if (@$info || Session::has('info'))
                <div class="alert alert-info alert-dismissible width-100-login">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <center><h4><i class="icon fa fa-info"></i> Info!</h4></center>
                        <center>{{ @$info ? @$info : session('info') }} </center>
                    </div>
                @endif
            </div>
            <div class="main-content">
                <div class="image">
                    <img src="{{ asset('/login-img/poltek-angkatan-darat.jpg')}}" width="100%" height="100%" alt="image">
                </div>
                <div class="main-form">
                    <div class="form-header">
                        <div class="logo">
                            <img src="{{ asset('/login-img/logo-pm.png')}}" width="50px" alt="">
                        </div>
                        <div class="text-header">
                            Silahkan Login Ke Dashboard
                        </div>
                    </div>
                    <form class="form-child" action="{{ route('handle.login.save')}}" method="POST">
                        @csrf
                        @method('POST')
                        <input type="email" name="email" placeholder="Email" autofocus>
                        <input type="password" name="password" placeholder="Password">
                        <button type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="{{ asset('/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>

</body>
</html>
