@extends('template')

@section('title')
Invoice Pelanggaran
@endsection

@section('activeDashboard')
active
@endsection

@section('pageName')
Invoice Pelanggaran
@endsection

@section('css')
<style>
    @font-face {
        font-family: 'Roboto-Regular';
        src: url("{{ asset('/fonts/Roboto/Roboto-Regular.ttf')}}") format("truetype");
        font-weight: 400; // use the matching font-weight here ( 100, 200, 300, 400, etc).
        font-style: normal; // use the matching font-style here
    }

    * {
        font-family: 'Roboto-Regular';
    }

    input,
    select,
    textarea {
        font-weight: bold;
    }

    input[disabled],
    textarea[disabled],
    select[disabled] {
        background-color: white;
    }

    input::-webkit-input-placeholder {
        /* Edge */
        color: grey;
        font-weight: 100;
        opacity: 0.8;
    }

    input:-ms-input-placeholder {
        /* Internet Explorer 10-11 */
        color: grey;
        font-weight: 100;
        opacity: 0.8;
    }

    input::placeholder {
        color: grey;
        font-weight: 100;
        opacity: 0.8;
    }

    input[type=checkbox] {
        transform: scale(1.3);
        -ms-transform: scale(1.3);
        -webkit-transform: scale(1.3);
        border: 0px !important;
    }

    .tabel-pekerjaan {
        font-weight: bold;
        font-size: 12px;
    }

    .isi-pelanggaran {
        text-align: left !important;
    }

    .jenis-pekerjaan[type=checkbox] {
        margin: 3px;
        margin-left: 25px;
    }

    .table-pelanggaran thead tr th,
    .table-pelanggaran tbody tr td {
        padding: 5px;
        text-align: center;
        font-size: 12px;
    }

    .table-indent {
        text-indent: 2em !important;
    }

    .text-opening-letter {
        font-size: 12px;
    }

    .table-data-diri tbody tr td {
        font-size: 12px;
    }

    .input-text-css-empty {
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
        text-align: center;
    }

    .input-num-css-empty {
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
        text-align: center;
    }

    .input-num-short-css-empty {
        text-align: center;
        width: 30px;
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }

    .input-text-long-css-empty {
        text-align: center;
        width: 200px;
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }

    .input-css-empty {
        text-align: center;
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }
    .text-align-left {
        text-align: left !important;
    }
    .text-padding-10 {
        padding-left: 10px !important;
    }
    @media print {
        body {
            width: auto;
            height: 65%;
        }
    }

    .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single{
        width: 200px!important;
        border-bottom: 1px dotted black!important;
        border-top: 0px!important;
        border-left: 0px!important;
        border-right: 0px!important;
        padding:0!important;
        height:auto!important;
        display:block;
        font-weight:bold;
    }

    .select2-container--default.select2-container--open{
        background:#fff;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-search--dropdown .select2-search__field{
        width:100%;
    }

    .select2-results__option{
        list-style:none;
    }

</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Print Catatan Pelanggaran
            <div style="float:right;">
                <a href="{{route('view.tutorial.print.pdf')}}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Tutorial Print PDF</a>
                <a href="{{route('view.catatan.pelanggaran')}}" class="btn btn-success">Kembali</a>
            </div>
        </h1>
    </section>
    <div class="pad margin no-print">
        <div class="callout callout-success" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Catatan:</h4>
            Tolong Isi Form (input yang ada titik<sup>2</sup>) yang sudah ada, apabila ingin mengisi sendiri silahkan
            dikosong kan. Tolong juga Sesuaikan apabila anda ingin mencetak halaman ini pada page setelah anda menekan tombol <b style="background-color: white; color: black;padding:5px;margin:5px;">Cetak PDF</b>
        </div>
    </div>
    <form action="{{route('handle.pelanggaran.create.save')}}" method="POST">
        @csrf
        @method('POST')
        <!-- Main content -->
        <section class="invoice" style="overflow: hidden;">
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <address style="font-size: 12px">
                        <center>
                            <strong>KOMANDO DAERAH MILITER XII/TANJUNGPURA</strong><br>
                            <strong>POLISI MILITER</strong><br>
                        </center>
                        <hr style="margin-top: 0px;margin-bottom: 0px;padding-bottom: 0px;">
                        <strong>UNTUK KEADILAN</strong><br>
                        <strong>NOMOR : <input name="untuk_keadilan_nomor" class="input-num-css-empty input-num-only text-align-left text-padding-10"
                                placeholder="Isi Nomor"></strong>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <table class="tabel-pekerjaan" style="float:right;">
                        <tbody>
                            <tr>
                                <td>ANGKATAN DARAT</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="1" type="checkbox"></td>
                            </tr>
                            <tr>
                                <td>ANGKATAN LAUT</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="2" type="checkbox"></td>
                            </tr>
                            <tr>
                                <td>ANGKATAN UDARA</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="3" type="checkbox"></td>
                            </tr>
                            <tr>
                                <td>PNS</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="4" type="checkbox"></td>
                            </tr>
                            <tr>
                                <td>SIPIL</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="5" type="checkbox"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12">
                    <center style="font-size: 12px;">
                        <strong>BERITA ACARA PELANGGARAN LALU LINTAS TERTENTU</strong><br>
                        <strong>(BALANG LALIN)</strong>
                    </center>
                </div>
            </div>
            <br>

            <div class="row"
                style="border: 1px solid black; border-bottom: 0px; margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding-top: 10px;">
                    <p class="text-indent text-opening-letter" style="text-align: justify;text-indent: 5em !important;">
                        Pada hari ini <input name="hari_ini_pembukaan" type="text" class="input-text-css-empty"
                            placeholder="Hari">
                        ,tanggal <input name="tanggal_pembukaan" class="input-text-css-empty input-num-only"
                            placeholder="Tanggal">
                        ,bulan <select name="bulan_pembukaan" class="input-text-css-empty"
                            placeholder="Bulan">
                                <option value='01' {{ date('m') == '01' ? "selected" : "" }}>Januari</option>
                                <option value='02' {{ date('m') == '02' ? "selected" : "" }}>Februari</option>
                                <option value='03' {{ date('m') == '03' ? "selected" : "" }}>Maret</option>
                                <option value='04' {{ date('m') == '04' ? "selected" : "" }}>April</option>
                                <option value='05' {{ date('m') == '05' ? "selected" : "" }}>Mei</option>
                                <option value='06' {{ date('m') == '06' ? "selected" : "" }}>Juni</option>
                                <option value='07' {{ date('m') == '07' ? "selected" : "" }}>Juli</option>
                                <option value='08' {{ date('m') == '08' ? "selected" : "" }}>Agustus</option>
                                <option value='09' {{ date('m') == '09' ? "selected" : "" }}>September</option>
                                <option value='10' {{ date('m') == '10' ? "selected" : "" }}>Oktober</option>
                                <option value='11' {{ date('m') == '11' ? "selected" : "" }}>November</option>
                                <option value='12' {{ date('m') == '12' ? "selected" : "" }}>Desember</option>
                            </select>
                        ,tahun <input name="tahun_pembukaan" class="input-text-css-empty input-num-only"
                            placeholder="Tahun">
                        ,sekira pukul <input name="pukul_jam_kejadian_pembukaan"
                            class="input-num-short-css-empty input-num-only" placeholder="Jam"> : <input
                            name="pukul_menit_kejadian_pembukaan" class="input-num-short-css-empty input-num-only"
                            placeholder="Menit">
                        di
                        jalan <input name="alamat_jalan" type="text" class="input-text-long-css-empty" placeholder="Nama Jalan" style="width:200px;">,
                        kota <input name="alamat_kota" type="text" class="input-text-css-empty" placeholder="Nama Kota">,
                        petugas yang bertanda tangan di bawah ini, berdasarkan undang-undang RI Nomor 31
                        Tahun 1997 tentang Peradilan Militer dan Keputusan Panglima TNI Nomor Kep/650/VII/2011
                        tentang Penyerahan Perkara Pelanggaran Lalu Lintas, telah Melakukan pemeriksaan terhadap
                        sebuah kendaraan jenis <input name="jenis_kendaraan" type="text"
                            class="input-text-long-css-empty" placeholder="Jenis Kendaraan">,
                        warna <input name="warna_kendaraan" type="text" class="input-text-css-empty" placeholder="Warna">
                        Nomor Registrasi <input name="nomor_kendaraan" type="text" class="input-text-long-css-empty" placeholder="Nomor Registrasi">
                        Yang dikemudikan oleh seorang
                        <select name="kelamin_pengendara" style="color: black; border:0px;outline:0px;padding:5px;" required>
                            <option selected disabled>Pilih kelamin</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                        <input type="hidden" name="member_id"/>
                    </p>
                </div>
                <div class="col-xs-12 table-responsive">
                    <table class="table-data-diri">
                        <tbody>
                            <tr>
                                <td>Name </td>
                                <td class="table-indent">:</td>
                                <td> <input class="input-text-long-css-empty" name="nama_pengendara" type="text"
 ]                                       value="" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Pangkat/NRP </td>
                                <td class="table-indent">:</td>
                                <td> <select id="select-member" class="input-text-long-css-empty select2select2-hidden-accessible" style="display:none" aria-hidden="true" name="id_member">
                                <option></option>
                                @if (@$data['member'])
                                @foreach ($data['member'] as $key => $m)
                                    <option value="{{$key}}">{{$m->pangkat}} / {{$m->nrp}}</option>
                                @endforeach
                                @endif
                            </select></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan/Jabatan </td>
                                <td class="table-indent">:</td>
                                <td> <input class="input-text-long-css-empty" name="pekerjaan_atau_jabatan_pengendara"
                                        type="text" value="" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Kesatuan </td>
                                <td class="table-indent">:</td>
                                <td> <input class="input-text-long-css-empty" name="kesatuan_pengendara" type="text"
                                        value="" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Alamat </td>
                                <td class="table-indent">:</td>
                                <td> <textarea class="input-text-long-css-empty" name="alamat_pengendara" type="text"
                                        style="text-align: justify;width: 300px; height:100px"
                                        readonly></textarea></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12" style="padding-top:10px;">
                    <p class="text-indent text-opening-letter" style="text-align: justify;">
                        Yang diduga atau patut diduga telah melakukan pelanggaran lalu lintas yang diatur dalam
                        UU
                        Nomor 22 Tahun 2009 tentang lalu lintas dan Angkutan jalan :
                    </p>
                </div>
            </div>

            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding:0px;">
                    <table class="table-pelanggaran" border="1" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">Pasal Yang Dilanggar</th>
                                <th style="text-align: center;">Jenis Pelanggaran</th>
                                <th style="text-align: center;">(V)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['master_pelanggaran'] as $p)
                            <tr>
                                <td>{{ $p->pasal }}</td>
                                <td class="isi-pelanggaran">{{ $p->name }}
                                </td>
                                <td>
                                    <input name="jenis_pelanggaran[]" type="checkbox" value="{{ $p->id}}" />
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>Lain-lain</td>
                                <td class="isi-pelanggaran"><textarea name="isi_pelanggaran_lain_lain" type="text"
                                        class="input-css-empty" style="width: 100%;text-align: left"></textarea></td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="lain" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                    <h4 style="margin-left: 0px; font-size: 12px; font-weight: bold;">Keterangan jenis pelanggaran</h4>
                    <hr style="margin-top: 15px;margin-bottom: 5px;">
                </div>
            </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;font-size: 12px;">
                    <p style="text-indent: 5em !important;"> Atas pelanggaran yang telah dilakukannya, kepada
                        Pelanggar
                        telah dibuatkan Berita Acara Pelanggaran Lalu Lintas dan kepadanya telah diberitahukan
                        untuk
                        mengahadap Hakim pada Peradilan Militer <input name="peradilan_militer" type="text"
                            class="input-css-empty" style="width: 300px;" placeholder="Peradilan Militer">
                        pada tanggal/waktu yang telah ditetapkan sesuai dengan surat panggilan.</p>
                    <p style="text-indent: 5em !important;"> Untuk kepentingan penyidik selanjutnya/menghentikan
                        pelanggaran yang telah terjadi, berdasarkan Pasal 260 UU No. 22 Tahun 2009, petugas
                        menahan/menyita untuk sementara : KENDARAAN/SIM/STNK/BENDA LAIN berupa :<br /> <input
                            name="menahan_atau_menyita" type="text" class="input-css-empty" style="width: 300px;"  placeholder="Sitaan">
                    </p>
                    <p style="text-indent: 5em !important;"> Demikian Berita Acara ini dibuat dengan sebenarnya,
                        kemudian untuk menguatkannya Tersangka, dan penyidik telah membubuhkan tanda tangan
                        seperti
                        tersebut dibawah ini :</p>
                </div>
            </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;font-size: 12px;">
                <div class="col-xs-4">
                    <h5><u><b>Tersangka</b></u></h5>
                    <h5>Tanda Tangan</h5>
                </div>
                <div class="col-xs-4">
                    <h5><u><b>Penyidik</b></u></h5>
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td class="table-indent">:</td>
                                <td> <input name="penyidik_nama" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Pangkat/NRP</td>
                                <td class="table-indent">:</td>
                                <td> <input name="penyidik_pangkat" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td class="table-indent">:</td>
                                <td> <input name="penyidik_jabatan" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Kesatuan</td>
                                <td class="table-indent">:</td>
                                <td> <input name="penyidik_kesatuan" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Tanda Tangan</td>
                                <td class="table-indent"></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-4">
                    <h5><u><b>Disahkan</b></u></h5>
                    <table>
                        <tbody>
                            <tr>
                                <td>Di</td>
                                <td class="table-indent">:</td>
                                <td> <input name="disahkan_di" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Pada Tanggal</td>
                                <td class="table-indent">:</td>
                                <td> <input name="disahkan_tanggal" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td class="table-indent">:</td>
                                <td> <input name="disahkan_nama" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Pangkat/NRP</td>
                                <td class="table-indent">:</td>
                                <td> <input name="disahkan_pangkat" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td class="table-indent">:</td>
                                <td> <input name="disahkan_jabatan" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Kesatuan</td>
                                <td class="table-indent">:</td>
                                <td> <input name="disahkan_kesatuan" type="text" class="input-text-long-css-empty text-align-left text-padding-10"></td>
                            </tr>
                            <tr>
                                <td>Tanda Tangan</td>
                                <td class="table-indent"></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12"><br><br><br><br><br><br><br><br></div>
                <div class="col-xs-12" style="font-size: 12px;">
                    <p>Catatan :</p>
                    <ol>
                        <li>Terhadap pelanggaran yang terjadi, pada kolom yang tersedia diberikan tanda silang.
                        </li>
                        <li>Balang Lalin ini berlaku pula sebagai bukti penyitaan.</li>
                        <li>Lembar Merah Untuk Pelanggar.</li>
                        <li>Lembar Kuning Untuk Ankum.</li>
                        <li>Lembar Hijau Untuk Dilmil/Dilmilti.</li>
                        <li>Lembar Biru Untuk Otmil/Otmilti.</li>
                        <li>Lembar Putih Untuk Arsip Kesatuan.</li>
                    </ol>
                </div>
            </div>
            <br>
            <div class="row no-print">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> SAVE</button>
                    <button type="submit" class="btn btn-success pull-right" style="margin-right: 5px;" name="cetak" value="1"><i class="fa fa-print"></i> CETAK PDF</button>
                <a href="{{route('view.pelanggaran.final.print')}}"  target="_blank" type="button" class="btn btn-warning pull-right" style="margin-right: 5px;"><i class="fa fa-sticky-note-o"></i> Cetak Template PDF (Kosong Data)</a>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </form>
    <div class="clearfix"></div>
</div>
@endsection
@section('script')
<script src="{{asset('/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script>
    (function ($) {
        var json_member = JSON.parse('{!! json_encode($data["member"]) !!}');
        $('#select-member').select2({
            placeholder: "Pilih Member",
            allowClear: true
        });

        $('#select-member').change(function(){
            var m_key = $(this).val();
            $("input[name=member_id]").val(json_member[m_key].id);
            $("input[name=nama_pengendara]").val(json_member[m_key].name);
            $("input[name=pekerjaan_atau_jabatan_pengendara]").val(json_member[m_key].jabatan);
            $("input[name=kesatuan_pengendara]").val(json_member[m_key].kesatuan);
            $("textarea[name=alamat_pengendara]").html(json_member[m_key].alamat_rumah);
        })

        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop",
                function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
        };
    }(jQuery));

    //input logic attribut
    $(".input-num-only").inputFilter(function (value) {
        return /^\d*$/.test(value);
    });
    $('.input-text-css-empty').attr('maxlength', '10').attr('style', 'width: 80px;').attr(
        'autocomplete', 'off')
    $('.input-num-css-empty').attr('style', 'width: 200px;').attr('autocomplete', 'off')

    // logic date
    var date = new Date();
    var day = date.getDay();
    var month = date.getMonth();
    var year = date.getFullYear();
    if(date.getDate() < 10){
        tgl = "0"+ date.getDate();
    } else {
        tgl = date.getDate();
    }
    var tanggal = tgl
    var jam = date.getHours()
    var menit = date.getMinutes()
    if(jam < 10) {
        jam='0'+jam
    }

    if(menit < 10){
        menit = '0'+menit
    }
    var dayNames = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
    var monthNames = ["January", "February", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"]

    //auto fill input tanggal dan jam
    $('input[name=hari_ini_pembukaan]').val(dayNames[day])
    $('input[name=tanggal_pembukaan]').val(tanggal)
    $('input[name=bulan_pembukaan]').val(monthNames[month])
    $('input[name=tahun_pembukaan]').val(year)
    $('input[name=pukul_jam_kejadian_pembukaan]').val(jam)
    $('input[name=pukul_menit_kejadian_pembukaan]').val(menit)

    $('.jenis-pekerjaan').on("click", function() {
        $(".jenis-pekerjaan").prop('checked', false);
        $(this).prop('checked', true);
    })
</script>
@endsection
