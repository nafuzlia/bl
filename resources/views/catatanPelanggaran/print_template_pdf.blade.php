<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{@$data['nama_pengendara'] ? 'Catatan Pelanggaran'.@$data['nama_pengendara']: 'Catatan Pelanggaran Template'}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">

    <style>
            @font-face {
                font-family: 'Roboto-Regular';
                src: url("{{ asset('/fonts/Roboto/Roboto-Regular.ttf')}}") format("truetype");
                font-weight: 400; // use the matching font-weight here ( 100, 200, 300, 400, etc).
                font-style: normal; // use the matching font-style here
            }

            * {
                font-family: 'Roboto-Regular';
                background-color: rgba(255, 255, 255, 0.05) !important;
            }

            input,
            select,
            textarea {
                font-weight: bold;
            }

            input[disabled],
            textarea[disabled],
            select[disabled] {
                background-color: white;
            }

            input::-webkit-input-placeholder {
                /* Edge */
                color: grey;
                font-weight: 100;
                opacity: 0.8;
            }

            input:-ms-input-placeholder {
                /* Internet Explorer 10-11 */
                color: grey;
                font-weight: 100;
                opacity: 0.8;
            }

            input::placeholder {
                color: grey;
                font-weight: 100;
                opacity: 0.8;
            }

            input[type=checkbox] {
                transform: scale(1.3);
                -ms-transform: scale(1.3);
                -webkit-transform: scale(1.3);
                border: 0px !important;
            }

            .tabel-pekerjaan {
                font-weight: bold;
                font-size: 12px;
            }

            .isi-pelanggaran {
                text-align: left !important;
            }

            .jenis-pekerjaan[type=checkbox] {
                margin: 3px;
                margin-left: 25px;
            }

            .table-pelanggaran thead tr th,
            .table-pelanggaran tbody tr td {
                padding: 5px;
                text-align: center;
                font-size: 12px;
            }

            .table-indent {
                text-indent: 2em !important;
            }

            .text-opening-letter {
                font-size: 12px;
            }

            .table-data-diri tbody tr td {
                font-size: 12px;
            }

            .input-text-css-empty {
                border-bottom: 1px dotted black;
                border-top: 0px;
                border-left: 0px;
                border-right: 0px;
            }

            .input-num-css-empty {
                border-bottom: 1px dotted black;
                border-top: 0px;
                border-left: 0px;
                border-right: 0px;
            }

            .input-num-short-css-empty {
                text-align: center;
                width: 30px;
                border-bottom: 1px dotted black;
                border-top: 0px;
                border-left: 0px;
                border-right: 0px;
            }

            .input-text-long-css-empty {
                text-align: center;
                width: 200px;
                border-bottom: 1px dotted black;
                border-top: 0px;
                border-left: 0px;
                border-right: 0px;
            }

            .input-css-empty {
                text-align: center;
                border-bottom: 1px dotted black;
                border-top: 0px;
                border-left: 0px;
                border-right: 0px;
            }
            @media print {
                body {
                    width: auto;
                    height: 65%;
                }
                * {-webkit-print-color-adjust:exact;}
            }
    </style>
</head>

<body>
    <center>
        <div class="image-watermark" style=" z-index: -3; position: absolute; opacity: 0.5; display: block; margin-left: auto; margin-right: auto; width: 95vw; height: auto; top: 500px;">
            <img src="{{asset('default/Logo.jpeg')}}" width="45%" height="50%" alt="">
        </div>
    </center>
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice" style="overflow: hidden;">
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        <address style="font-size: 12px">
                            <center>
                                <strong>KOMANDO DAERAH MILITER XII/TANJUNGPURA</strong><br>
                                <strong>POLISI MILITER</strong><br>
                            </center>
                            <hr style="margin-top: 0px;margin-bottom: 0px;padding-bottom: 0px;">
                            <strong>UNTUK KEADILAN</strong><br>
                        <strong>NOMOR : <input name="untuk_keadilan_nomor" value="{{@$data['untuk_keadilan_nomor']}}" class="input-num-css-empty input-num-only" ></strong>
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <table class="tabel-pekerjaan" style="float:right;">
                            <tbody>
                                <tr>
                                    <td>ANGKATAN DARAT</td>
                                    <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" type="checkbox" {{@(in_array('1', @$data['jenis_pekerjaan']) ? 'checked' : '')}}></td>
                                </tr>
                                <tr>
                                    <td>ANGKATAN LAUT</td>
                                    <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" type="checkbox" {{@(in_array('2', @$data['jenis_pekerjaan']) ? 'checked' : '')}}></td>
                                </tr>
                                <tr>
                                    <td>ANGKATAN UDARA</td>
                                    <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" type="checkbox" {{@(in_array('3', @$data['jenis_pekerjaan']) ? 'checked' : '')}}></td>
                                </tr>
                                <tr>
                                    <td>PNS</td>
                                    <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" type="checkbox" {{@(in_array('4', @$data['jenis_pekerjaan']) ? 'checked' : '')}}></td>
                                </tr>
                                <tr>
                                    <td>SIPIL</td>
                                    <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" type="checkbox" {{@(in_array('5', @$data['jenis_pekerjaan']) ? 'checked' : '')}}></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                    <div class="col-xs-12">
                        <center style="font-size: 14px;">
                            <strong>BERITA ACARA PELANGGARAN LALU LINTAS TERTENTU</strong><br>
                            <strong>(BALANG LALIN)</strong>
                        </center>
                    </div>
                </div>
                <br>
                <div class="row"
                    style="border: 1px solid black; border-bottom: 0px; margin-left: 10px; margin-right: 10px;">
                    <div class="col-xs-12" style="padding-top: 10px;">
                        <p class="text-indent text-opening-letter" style="text-align: justify;text-indent: 5em !important;">
                            Pada hari ini <input name="hari_ini_pembukaan" value="{{@$data['hari_ini_pembukaan']}}" type="text" class="input-text-css-empty"
                                >
                            ,tanggal <input name="tanggal_pembukaan" value="{{@$data['tanggal_pembukaan']}}" class="input-text-css-empty input-num-only"
                                >
                            ,bulan <input name="bulan_pembukaan" value="{{@$data['bulan_pembukaan']}}" type="text" class="input-text-css-empty"
                                >
                            ,tahun <input name="tahun_pembukaan" value="{{@$data['tahun_pembukaan']}}" class="input-text-css-empty input-num-only"
                                >
                            ,sekira pukul <input name="pukul_jam_kejadian_pembukaan"
                                class="input-num-short-css-empty input-num-only" value="{{@$data['pukul_jam_kejadian_pembukaan']}}"> : <input
                                name="pukul_menit_kejadian_pembukaan" value="{{@$data['pukul_menit_kejadian_pembukaan']}}" class="input-num-short-css-empty input-num-only"
                                >
                            di
                            jalan <input name="alamat_jalan" type="text" class="input-text-long-css-empty" value="{{@$data['alamat_jalan']}}" >,
                            kota <input name="alamat_kota" type="text" class="input-text-css-empty" value="{{@$data['alamat_kota']}}" >,
                            petugas yang bertanda tangan di bawah ini, berdasarkan undang-undang RI Nomor 31
                            Tahun 1997 tentang Peradilan Militer dan Keputusan Panglima TNI Nomor Kep/650/VII/2011
                            tentang Penyerahan Perkara Pelanggaran Lalu Lintas, telah Melakukan pemeriksaan terhadap
                            sebuah kendaraan jenis <input name="jenis_kendaraan" type="text"
                                class="input-text-long-css-empty" value="{{@$data['jenis_kendaraan']}}">,
                            warna <input name="warna_kendaraan" type="text" class="input-text-css-empty" value="{{@$data['warna_kendaraan']}}" >
                            Nomor Registrasi <input name="nomor_kendaraan" type="text" class="input-text-long-css-empty" value="{{@$data['nomor_kendaraan']}}">
                            Yang dikemudikan oleh seorang <b>{{@$data['kelamin_pengendara']}}</b> :
                        </p>
                    </div>
                    <div class="col-xs-12 table-responsive">
                        <table class="table-data-diri">
                            <tbody>
                                <tr>
                                    <td>Name </td>
                                    <td class="table-indent">:</td>
                                    <td style="width: 100%"><b style="padding-left:15px"> {{@$data['nama_pengendara']}}<br><hr style="border:0.5px dotted black;width: 100%; margin: 0px; margin-bottom: -15px"></b></td>
                                </tr>
                                <tr>
                                    <td>Pangkat/NRP </td>
                                    <td class="table-indent"> : </td>
                                    <td style="width: 100%"><b style="padding-left:15px"> {{@$data['pangkat_pengendara']}}<br><hr style="border:0.5px dotted black;width: 100%; margin: 0px; margin-bottom: -15px"></b></td>
                                </tr>
                                <tr>
                                    <td>Pekerjaan/Jabatan </td>
                                    <td class="table-indent"> : </td>
                                    <td style="width: 100%"><b style="padding-left:15px"> {{@$data['pekerjaan_atau_jabatan_pengendara']}}<br><hr style="border:0.5px dotted black;width: 100%; margin: 0px; margin-bottom: -15px"></b></td>
                                </tr>
                                <tr>
                                    <td>Kesatuan </td>
                                    <td class="table-indent"> : </td>
                                    <td style="width: 100%"><b style="padding-left:15px"> {{@$data['kesatuan_pengendara']}}<br><hr style="border:0.5px dotted black;width: 100%; margin: 0px; margin-bottom: -15px"></b></td>
                                </tr>
                                <tr>
                                    <td>Alamat </td>
                                    <td class="table-indent"> : </td>
                                    <td style="width: 100%"><b style="padding-left:15px"> {{@$data['alamat_pengendara']}}<br><hr style="border:0.5px dotted black;width: 100%; margin: 0px; margin-bottom: -15px"></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12" style="padding-top:10px;">
                        <p class="text-indent text-opening-letter" style="text-align: justify;">
                            Yang diduga atau patut diduga telah melakukan pelanggaran lalu lintas yang diatur dalam
                            UU
                            Nomor 22 Tahun 2009 tentang lalu lintas dan Angkutan jalan :
                        </p>
                    </div>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                    <div class="col-xs-12" style="padding:0px;">
                        <table class="table-pelanggaran" border="1" style="border-collapse: collapse;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Pasal Yang Dilanggar</th>
                                    <th style="text-align: center;">Jenis Pelanggaran</th>
                                    <th style="text-align: center;">(X)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>278</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor beroda empat atau lebih di jalan
                                        yang
                                        tidak
                                        dilengkapi dengan perlengkapan
                                        berupa ban cadangan, segitiga, dongkrak, pembuka roda dan peralatan PPPK.
                                    </td>
                                    <td>
                                        <input name="jenis_pelanggaran[]" type="checkbox" value="1" {{@(in_array('1', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>280</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang tidak dipasangi
                                        tanda
                                        Nomor Ranmor
                                        yang di tetapkan</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="2" {{@(in_array('2', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>281</td>
                                    <td class="isi-pelanggaran">Mengemudi Ranmor di jalan yang tidak memiliki SIM.
                                    </td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="3" {{@(in_array('3', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>283</td>
                                    <td class="isi-pelanggaran">Mengemudi Ranmor di jalan secara tidak wajar &
                                        melakukan
                                        kegiatan lain
                                        atau di pengaruhi oleh suatu
                                        keadaan yang mengakibatkan gangguan konsentrasi dalam mengemudi di jalan.
                                    </td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="4" {{@(in_array('4', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>284</td>
                                    <td class="isi-pelanggaran">Mengemudi Ranmor dengan tidak mengutamakan
                                        keselamatan
                                        Pejalan kaki
                                        atau bersepeda.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="5" {{@(in_array('5', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>285(1)</td>
                                    <td class="isi-pelanggaran">Mengemudikan sepeda motor di jalan yang tidak
                                        memenuhi
                                        persyaratan
                                        teknis dan layak jalan yang
                                        meliputi kaca spion, klakson, lampu utama, lampu rem, lampu petunjuk arah,
                                        alat
                                        pemantul
                                        cahaya,
                                        alat pengukur kecepatan, knalpot dan kedalaman alur ban.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="6" {{@(in_array('6', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>285(2)</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor beroda empat atau lebih di jalan
                                        tidak
                                        memenuhi
                                        persyaratan teknis yang meliputi
                                        kaca spion, klakson, lampu utama, lampu rem, lampu tanda batas dimensi badan
                                        kendaraan,
                                        lampu
                                        gandingan, lampu rem, lampu petunjuk arah, alat pemantul cahaya, alat
                                        pengukur
                                        kecepatan,
                                        kedalaman
                                        alur ban, kaca depan spakbor, bumper, penggandengan, penempelan atau
                                        penghapus kaca.
                                    </td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="7" {{@(in_array('7', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>287</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang melanggar aturan
                                        perintah
                                        atau
                                        larangan yang dinyatakan dengan
                                        Rambu lalu lintas / Marka jalan.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="8" {{@(in_array('8', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>288(1)</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang tidak dilengkapi
                                        dengan
                                        STNK atau
                                        STCKB yang ditetapkan.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="9" {{@(in_array('9', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>288(2)</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang tidak dapat
                                        menunjukan SIM
                                        yang sah.
                                    </td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="10" {{@(in_array('10', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>289</td>
                                    <td class="isi-pelanggaran">Mengemudikan Ranmor atau Penumpang yang duduk di
                                        samping
                                        Pengemudi
                                        yang tidak mengenakan sabuk
                                        keselamatan.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="11" {{@(in_array('11', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>291(1)</td>
                                    <td class="isi-pelanggaran">Mengemudikan Sepeda Motor tidak mengenakan helm
                                        Standar
                                        Nasional
                                        Indonesia.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="12" {{@(in_array('12', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>292(2)</td>
                                    <td class="isi-pelanggaran">Mengemudikan sepeda Motor yang membiarkan
                                        penumpangnya tidak
                                        menggunakan Helm. </td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="13" {{@(in_array('13', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>293(2)</td>
                                    <td class="isi-pelanggaran">Mengemudikan Sepeda Motor di jaaln tanpa menyalakan
                                        lampu
                                        utama pada
                                        siang hari.</td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="14" {{@(in_array('14', @$data['jenis_pelanggaran']) ? 'checked' : '')}}/></td>
                                </tr>
                                <tr>
                                    <td>Lain-lain</td>
                                    <td class="isi-pelanggaran" style="font-weight: bold;">{{@$data['isi_pelanggaran_lain_lain']}}<br><hr style="border:0.5px dotted black;width: 100%; margin: 0px"></td>
                                    <td><input name="jenis_pelanggaran[]" type="checkbox" value="lain" {{@(in_array('lain', @$data['jenis_pelanggaran']) ? 'checked' : '')}} /></td>
                                </tr>
                            </tbody>
                        </table>
                    </img>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                    <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                        <h4 style="margin-left: 0px; font-size: 12px; font-weight: bold;">Keterangan jenis pelanggaran</h4>
                        <hr style="margin-top: 15px;margin-bottom: 5px;">
                    </div>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                    <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;font-size: 12px;">
                        <p style="text-indent: 5em !important;"> Atas pelanggaran yang telah dilakukannya, kepada
                            Pelanggar
                            telah dibuatkan Berita Acara Pelanggaran Lalu Lintas dan kepadanya telah diberitahukan
                            untuk
                            mengahadap Hakim pada Peradilan Militer <input name="peradilan_militer" type="text"
                        class="input-css-empty" style="width: 300px;" value="{{@$data['peradilan_militer']}}">
                            pada tanggal/waktu yang telah ditetapkan sesuai dengan surat panggilan.</p>
                        <p style="text-indent: 5em !important;"> Untuk kepentingan penyidik selanjutnya/menghentikan
                            pelanggaran yang telah terjadi, berdasarkan Pasal 260 UU No. 22 Tahun 2009, petugas
                            menahan/menyita untuk sementara : KENDARAAN/SIM/STNK/BENDA LAIN berupa :<br /> <input
                                name="menahan_atau_menyita" type="text" class="input-css-empty" value="{{@$data['menahan_atau_menyita']}}" style="width: 300px;">
                        </p>
                        <p style="text-indent: 5em !important;"> Demikian Berita Acara ini dibuat dengan sebenarnya,
                            kemudian untuk menguatkannya Tersangka, dan penyidik telah membubuhkan tanda tangan
                            seperti
                            tersebut dibawah ini :</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px;font-size: 12px;">
                    <div class="col-xs-4">
                        <h5><u><b>Tersangka</b></u></h5>
                        <h5>Tanda Tangan</h5>
                    </div>
                    <div class="col-xs-4">
                        <h5><u><b>Penyidik</b></u></h5>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="penyidik_nama" type="text" class="input-text-long-css-empty" value="{{@$data['penyidik_nama']}}"></td>
                                </tr>
                                <tr>
                                    <td>Pangkat/NRP</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="penyidik_pangkat" type="text" class="input-text-long-css-empty" value="{{@$data['penyidik_pangkat']}}"></td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="penyidik_jabatan" type="text" class="input-text-long-css-empty" value="{{@$data['penyidik_jabatan']}}"></td>
                                </tr>
                                <tr>
                                    <td>Kesatuan</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="penyidik_kesatuan" type="text" class="input-text-long-css-empty" value="{{@$data['penyidik_kesatuan']}}"></td>
                                </tr>
                                <tr>
                                    <td>Tanda Tangan</td>
                                    <td class="table-indent"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-4">
                        <h5><u><b>Disahkan</b></u></h5>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Di</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="disahkan_nama" type="text" class="input-text-long-css-empty" value="{{@$data['disahkan_nama']}}"></td>
                                </tr>
                                <tr>
                                    <td>Pada Tanggal</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="disahkan_nama" type="text" class="input-text-long-css-empty" value="{{@$data['disahkan_nama']}}"></td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="disahkan_nama" type="text" class="input-text-long-css-empty" value="{{@$data['disahkan_nama']}}"></td>
                                </tr>
                                <tr>
                                    <td>Pangkat/NRP</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="disahkan_pangkat" type="text" class="input-text-long-css-empty" value="{{@$data['disahkan_pangkat']}}"></td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="disahkan_jabatan" type="text" class="input-text-long-css-empty" value="{{@$data['disahkan_jabatan']}}"></td>
                                </tr>
                                <tr>
                                    <td>Kesatuan</td>
                                    <td class="table-indent">:</td>
                                    <td> <input name="disahkan_kesatuan" type="text" class="input-text-long-css-empty" value="{{@$data['disahkan_kesatuan']}}"></td>
                                </tr>
                                <tr>
                                    <td>Tanda Tangan</td>
                                    <td class="table-indent"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12"></div>
                    <div class="col-xs-12" style="font-size: 10px;">
                        <p>Catatan :</p>
                        <ol>
                            <li>Terhadap pelanggaran yang terjadi, pada kolom yang tersedia diberikan tanda silang.
                            </li>
                            <li>Balang Lalin ini berlaku pula sebagai bukti penyitaan.</li>
                            <li>Lembar Merah Untuk Pelanggar.</li>
                            <li>Lembar Kuning Untuk Ankum.</li>
                            <li>Lembar Hijau Untuk Dilmil/Dilmilti.</li>
                            <li>Lembar Biru Untuk Otmil/Otmilti.</li>
                            <li>Lembar Putih Untuk Arsip Kesatuan.</li>
                        </ol>
                    </div>
                </div>
        </section>
        <!-- /.content -->
    <!-- REQUIRED JS SCRIPTS -->
    </div>
    <!-- jQuery 3 -->
    <script src="{{ asset('/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            window.print()
        });
        (function ($) {
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop",
            function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));
        $(".input-num-only").inputFilter(function(value) {
            return /^\d*$/.test(value);
        });
        $('.input-text-css-empty').attr('maxlength', '10').attr('style', 'text-align:center; width: 80px;').attr('autocomplete', 'off')
        $('.input-num-css-empty').attr('style', 'text-align:center; width: 200px;').attr('autocomplete', 'off')
        $('input, select, textarea').attr('readonly',true)
        $('input[type="checkbox"]').attr('onclick','return false;')
    </script>
</body>

</html>
