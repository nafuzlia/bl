@extends('template')

@section('title')
Detail Catatan Pelanggaran
@endsection

@section('activeDashboard')
active
@endsection

@section('pageName')
Detail Catatan Pelanggaran
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{ asset('/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/plugins/iCheck/all.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
        </center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
        </center>
        <center>{{ @$success ? @$success : session('alert') }} </center>
    </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-info"></i> Info!</h4>
        </center>
        <center>{{ @$info ? @$info : session('info') }} </center>
    </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Detail catatan Pelanggaran</h3>
            <div style="float: right;">
            @if ((boolean)Session::get('is_admin'))
                @if (@$data['catatan'])
                    @foreach ($data['catatan'] as $key => $value)
                        <a class="btn btn-info" href="{{route('view.pelanggaran.print', $value->id)}}"><i class="fa fa-file-pdf-o"></i> Print PDF</a>
                    @endforeach
                @endif
            @endif
                <a class="btn btn-success" href="{{route('view.catatan.pelanggaran')}}"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal" action="{{route('handle.pelanggaran.create.save')}}" method="POST"
                enctype="multipart/form-data">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Member</label>
                        @if (@$data['catatan'])
                            @foreach ($data['catatan'] as $key => $value)
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ $value->name }}"
                                            placeholder="Nama" disabled>
                                </div>
                                <div class="col-sm-2">
                                    <a href="{{ route('view.member.detail', $value->id_member) }}" class="btn btn-danger"
                                        style="width:100%"><i class="fa fa-users"></i> Detail Member</a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Pelanggaran</label>
                        <div class="col-sm-10">
                            @if (@$data['data_master'])
                            @foreach ($data['data_master'] as $key)
                            <div class="row">
                                <div class="col-sm-1"
                                    style="text-align:center;padding-right:0px !important; margin-right:0px !important;">
                                    {{$loop->iteration}}. </div>
                                <div class="col-sm-9" style="padding-left:0px !important; margin-left:0px !important;">
                                    <p style="border:0px;font-size:15px;font-weight:500;padding:5px;background-color:#FFFF;width:100%;"
                                        disabled>{{$key->name}}</p>
                                </div>
                                <div class="col-sm-2">
                                    <center>
                                        <label>
                                            <input type="checkbox" class="flat-red" value="{{$key->id}}"
                                                name="id_master[]" checked readonly>
                                        </label>
                                    </center>
                                </div>
                            </div>
                            <hr>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    @if (@$data['catatan'])
                    @foreach ($data['catatan'] as $key => $value)
                    <label for="inputName" class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        <input type="text" name="tgl" class="form-control pull-right" value="{{date('d F Y',strtotime($value->tgl))}}" id="datepicker"
                                autocomplete="off" disabled>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection

@section('script')
<script src="{{asset('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('/bower_components/admin-lte/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    //Initialize Select2 Elements
    $('.select2').select2()
    $('#select2-id_member-lj-result-3zbg-Pilih Member').remove()
    search = $('.select2-results__option').val();
    if (search == "Searching…") {

    }
    $('#datepicker').datepicker({
        autoclose: true
    })

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    })

</script>
@endsection
