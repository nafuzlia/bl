@extends('template')

@section('title')
Create Pelanggaran
@endsection

@section('activeDashboard')
active
@endsection

@section('pageName')
Buat Catatan Pelanggaran
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="{{ asset('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
        </center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
        </center>
        <center>{{ @$success ? @$success : session('alert') }} </center>
    </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-info"></i> Info!</h4>
        </center>
        <center>{{ @$info ? @$info : session('info') }} </center>
    </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Update Catatan Pelanggaran</h3>
            <div style="float: right;">
                <a class="btn btn-success" href="{{route('view.member.detail', [ $data['id_member'] ])}}"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal" action="{{route('handle.pelanggaran.update.save')}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="hidden" name="id_pelanggaran" value="{{(int)@$data['data_member']['catatan'][0]->{'id'} }}">
                <input type="hidden" name="id_detail_pelanggaran" value="{{(int)@$data['data_member']['catatan'][0]->{'id_detail'} }}">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Member</label>
                        <div class="col-sm-10">
                            <select id="select-member" class="select2 form-control select2-hidden-accessible" style="width: 100%;" aria-hidden="true" name="id_member">
                                <option></option>
                                @if (@$data['data_master']['member'])
                                @foreach ($data['data_master']['member'] as $key)
                                    <option value="{{$key->id}}" {{ @$key->id === (int)@$data['data_member']['catatan'][0]->{'id_member'} ? 'selected' : '' }}>{{$key->name}} / {{$key->nrp}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Pelanggaran</label>
                        <div class="col-sm-10">
                            @if (@$data['data_master']['masterPelanggaran'])
                            @foreach ($data['data_master']['masterPelanggaran'] as $key)
                                <div class="row">
                                    <div class="col-sm-1" style="text-align:center;padding-right:0px !important; margin-right:0px !important;">{{$loop->iteration}}. </div>
                                    <div class="col-sm-9" style="padding-left:0px !important; margin-left:0px !important;">
                                        <p style="border:0px;font-size:15px;font-weight:500;padding:5px;background-color:#FFFF;width:100%;" disabled>{{$key->name}}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <center>
                                            <label>
                                                <input type="checkbox" class="flat-red" value="{{$key->id}}" name="id_master[]" {{@(in_array($key->id, @$data['data_member']['id_data_master']) ? 'checked' : '')}}>
                                            </label>
                                        </center>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                            @endif
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Tanggal Pelanggaran</label>
                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            <input type="text" name="tgl" class="form-control pull-right" id="datepicker" value="{{ date('d F Y', strtotime(@$data['data_member']['catatan'][0]->{'tgl'})) }}"
                                    autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="" style="float:right;">
                        <a href="{{route('view.member.detail', [ $data['id_member'] ])}}" type="button" class="btn btn-primary">Kembali</a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
                <!-- /.box-body -->
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection
@section('script')
<script src="{{asset('/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('/bower_components/admin-lte/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script>
    //Initialize Select2 Elements
    $(document).ready(function() {
        $('#select-member').select2({
            placeholder: "Pilih Member",
            allowClear: true
        })

        $('#datepicker').datetimepicker({
            format: 'DD MMMM YYYY' //HH:mm:ss
        })

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })
    });
</script>
@endsection
