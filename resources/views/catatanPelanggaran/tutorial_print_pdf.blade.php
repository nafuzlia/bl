@extends('template')

@section('title')
Tutorial Print PDF
@endsection

@section('activeDashboard')
active
@endsection

@section('pageName')
Invoice Pelanggaran
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tutorial Print PDF Catatan Pelanggaran
        </h1>
    </section>
    <!-- Main content -->
    <section class="invoice" style="overflow: hidden;">
        <div class="row">
            <div class="col-xs-12">
                <h4 style="font-weight: bold;">1. Form Data (Isi Data , dan Ada Versi Kosong nya)</h4>
                <center>
                    <img src="{{asset('/print_tutorial/form_1_jangan_dikosongkan.png')}}" width="800">
                    <br>
                    <hr>
                    <img src="{{asset('/print_tutorial/form_2_jangan_dikosongkan.png')}}" width="800">
                    <br>
                    <hr>
                    <img src="{{asset('/print_tutorial/form_3_jangan_dikosongkan.png')}}" width="800">
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h4 style="font-weight: bold;">2. Tombol</h4>
                <p style="font-weight: lighter; padding-left: 25px">Klik Tombol Yang Disediakan :<br>1. <button class="btn btn-info"><i class="fa fa-print"></i> Cetak PDF</button> : Untuk Pergi Kehalaman print sesuai dengan apa yang telah di isi di form atas.<br>
                    2. <button class="btn btn-success"><i class="fa fa-sticky-note-o"></i> Cetak Template PDF (Kosong Data)</button> : Untuk Pergi Kehalaman Print , ini akan mencetak template surat(surat kosong).
                </p>
                <center>
                    <br>
                    <img src="{{asset('/print_tutorial/tombol_print.png')}}" width="800">
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h4 style="font-weight: bold;">3. Halaman Cetak Awal</h4>
                <center>
                    <img src="{{asset('/print_tutorial/tampilan_awal_print_page.png')}}" width="800">
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h4 style="font-weight: bold;">3. Halaman Cetak Konfigurasi</h4>
                <p style="font-weight: lighter; padding-left: 25px">
                    Silahkan Konfigurasi Sesuai Kertas Anda , dan Sesuaikan Besarnya Kontennya.<br>
                    Bila kepencet keluar Silahkan pencet <b>CTRL + P</b>.<br>
                    Sesuaikan Print Di <b>More Option</b>.<br>
                    ( Setting Sesuai Apa yang Dibutuhkan )
                    1. Paper Size (Besaran Kertas) <b>A4(bisa ke letter)</b>.<br>
                    2. Page Per Sheet (Banyaknya page) <b>1</b>.<br>
                    2. Margin (Jarak text konten dengan tepi)  <b>Default(1cm)</b>.<br>
                    2. Scale (Besar text konten dengan kertas) <b>65</b>.<br>
                    Contoh ada digambar :
                </p>
                <center>
                    <img src="{{asset('/print_tutorial/print_page_0.png')}}" width="800">
                    <br>
                    <hr>
                    <img src="{{asset('/print_tutorial/konfigurasi_print_page_ A4_PDF.png')}}" width="800">
                </center>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection
