@extends('template')

@section('title')
Dashboard
@endsection

@section('activeDashboard')
active
@endsection

@section('pageName')
Dashboard Catatan Pelanggaran
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<style>
    .dataTables_filter{
        display: none;
    }
</style>
@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid content900">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
        </center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
        </center>
        <center>{{ @$success ? @$success : session('alert') }} </center>
    </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-info"></i> Info!</h4>
        </center>
        <center>{{ @$info ? @$info : session('info') }} </center>
    </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title" style="margin-bottom: 50px;">Daftar Catatan Pelanggaran</h3>
            <div class="row">
            <div class="col-12 col-md-8">
                <input type="text" id="nrp_input_id" style="padding:20px; padding-left: 30px; width: 500px; border-radius: 50px; border: 1px solid grey; font-weight: bold; color:black;" placeholder="NRP">
            </div>
            <div class="col-12 col-md-4">
                <h5 style="font-weight: bold;color:red;">Sesuaikan Tanggal Pencarian</h5>
                <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <input type="text" name="from_date" style="font-size: 15px;padding: 30px;min-width: 200px;border-radius: 20px;" id="from_date" class="form-control" placeholder="Tanggal" autocomplete="off"/>
                    </div>
                    <div class="col-sm-6 col-lg-6" style="margin-top: 5px;">
                        <button type="button" name="filter" style="padding: 15px;font-size: 15px; border-radius: 20px;" id="sesuaikan" class="btn btn-primary">Sesuaikan</button>
                    </div>
                </div>

                @if ((boolean)Session::get('is_admin'))
                    <a class="btn btn-success btn-add-new-catatan-pelanggaran" style="margin-top: 20px; padding: 15px; border: 0; border-radius: 30px; z-index: 1;"  href="{{ route('view.pelanggaran.create')}}"><i class="fa fa-plus"></i> Add New Catatan Pelanggaran</a>
                @endif

            </div>
            </div>
        </div>
        <div class="box-header" style="background-color: rgba(255,255,255,0.0); margin-left: 20px;">
            <div style="float: left;">
                <h3 class="text-black" style="font-weight: bold;">Daftar Semua Acara pelanggaran Lalu Lintas</h3>
                <p class="text-danger" style="font-weight: bold;float:left;margin-right:50px; color:red;" id="pelanggar-total"></p><p class="text-primary" style="font-weight: bold;float:left;" id="lengkap-total"></p>
            </div>
            <div style="float: right;">
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-responsive table-striped display custome-table data-table"
                id="data-table" style="width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th style="border-right: 0px !important;">Daftar Anggota</th>
                        <th style="border-left:0px !important; border-right: 0px !important;"></th>
                        <th style="border-left:0px !important; border-right: 0px !important;"></th>
                        <th style="border-left:0px !important;"></th>
                        <th>Tanggal</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="t-body">
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{ asset('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ asset('/bower_components/fastclick/lib/fastclick.js')}}"></script>
<script src="{{ asset('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        var tgl = new Date();

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        $('#from_date').datepicker({
            todayBtn: 'linked',
            format: 'd MM yyyy',
            autoclose: true
        });

        var now = tgl.getDate()+" "+monthNames[tgl.getMonth()]+" "+tgl.getFullYear();

        $('#from_date').val(); //add now when u want to show with a date now, example : $('#from_date').val(now);

        load_data(); //add now when u want to show with a date now, example : load_data(now);

        function load_data(from_date = '') {
            $.ajax({
                url: "{{ route('view.catatan.pelanggaran') }}",
                data: {from_date:from_date, to_date:from_date},
                success: function (resp) {
                    const { data } = resp

                    const total_pelanggar = data.filter(data_value => {
                        return data_value.status.match('Melanggar')
                    }).length

                    const total_lengkap = data.filter(data_value => {
                        return data_value.status.match('Lengkap')
                    }).length

                    $('#pelanggar-total').text(`${total_pelanggar} orang Melanggar`)
                    $('#lengkap-total').text(`${total_lengkap} orang Lengkap`)
                }
            })
            $('.data-table').DataTable({
                retrieve: true,
                autoWidth: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('view.catatan.pelanggaran') }}",
                    data: {from_date:from_date, to_date:from_date},
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false
                    },
                    {
                        data: 'foto',
                        name: 'foto',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nrp',
                        name: 'nrp',
                        orderable: false,
                        searchable: true
                    },
                    {
                        data: 'name',
                        name: 'name',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'pangkat',
                        name: 'pangkat',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: false,
                        searchable: false
                    }
                ]
            })
        }

        $('#from_date').change(function () {
            var date = $('#from_date').val();
            $('.data-table').DataTable().destroy();
            $("#nrp_input_id").val("");
            load_data(date);
        });

        $('#sesuaikan').click(function () {
            $('#from_date').val(now)
            $('.data-table').DataTable().destroy();
            $("#nrp_input_id").val("");
            load_data(now);
        });

        var t = $('.data-table').DataTable();

        function filterGlobal () {
            $('.data-table').DataTable().search(
                $('#nrp_input_id').val()
            ).draw();
            $('.btn-add-new-catatan-pelanggaran').attr('href', '{{ route('view.pelanggaran.create') }}'+'?nrp='+$('#nrp_input_id').val())
        }
        $('#nrp_input_id').on( 'keyup click', function () {
            filterGlobal();
        });
    });
</script>
@endsection
