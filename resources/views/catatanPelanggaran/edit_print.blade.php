@extends('template')

@section('title')
Invoice Pelanggaran
@endsection

@section('activeDashboard')
active
@endsection

@section('pageName')
Invoice Pelanggaran
@endsection

@section('css')
<style>
    @font-face {
        font-family: 'Roboto-Regular';
        src: url("{{ asset('/fonts/Roboto/Roboto-Regular.ttf')}}") format("truetype");
        font-weight: 400; // use the matching font-weight here ( 100, 200, 300, 400, etc).
        font-style: normal; // use the matching font-style here
    }

    * {
        font-family: 'Roboto-Regular';
    }

    input,
    select,
    textarea {
        font-weight: bold;
    }

    input[disabled],
    textarea[disabled],
    select[disabled] {
        background-color: white;
    }

    input::-webkit-input-placeholder {
        /* Edge */
        color: grey;
        font-weight: 100;
        opacity: 0.8;
    }

    input:-ms-input-placeholder {
        /* Internet Explorer 10-11 */
        color: grey;
        font-weight: 100;
        opacity: 0.8;
    }

    input::placeholder {
        color: grey;
        font-weight: 100;
        opacity: 0.8;
    }

    input[type=checkbox] {
        transform: scale(1.3);
        -ms-transform: scale(1.3);
        -webkit-transform: scale(1.3);
        border: 0px !important;
    }

    .tabel-pekerjaan {
        font-weight: bold;
        font-size: 12px;
    }

    .isi-pelanggaran {
        text-align: left !important;
    }

    .jenis-pekerjaan[type=checkbox] {
        margin: 3px;
        margin-left: 25px;
    }

    .table-pelanggaran thead tr th,
    .table-pelanggaran tbody tr td {
        padding: 5px;
        text-align: center;
        font-size: 12px;
    }

    .table-indent {
        text-indent: 2em !important;
    }

    .text-opening-letter {
        font-size: 12px;
    }

    .table-data-diri tbody tr td {
        font-size: 12px;
    }

    .input-text-css-empty {
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }

    .input-num-css-empty {
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }

    .input-num-short-css-empty {
        text-align: center;
        width: 30px;
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }

    .input-text-long-css-empty {
        text-align: center;
        width: 200px;
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }

    .input-css-empty {
        text-align: center;
        border-bottom: 1px dotted black;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
    }
    @media print {
        body {
            width: auto;
            height: 65%;
        }
    }

    @media all and (min-width: 480px) and (max-width: 767px){
        .margin-top-edit-print {
            padding-top: 12.5px;
        }
    }
    @media all and (max-width: 599px) {
        .margin-top-edit-print {
            padding-top: 12.5px;
        }
    }
    @media all and (max-width: 479px) {
        .margin-top-edit-print {
            padding-top: 12.5px;
        }
        .btn-top-edit-print {
            margin: 5px;
        }
        .margin-top-bg-edit-print {
            margin-top: 50px !important;
        }
    }

    .text-indent-10 {
        text-indent: 10px;
    }
    .text-capitalize {
        text-transform: capitalize;
    }
</style>
@endsection

@section('content')
<?php
function getDay($hari){

	switch($hari){
		case 'Sun':
			$hari_ini = "Minggu";
		break;

		case 'Mon':
			$hari_ini = "Senin";
		break;

		case 'Tue':
			$hari_ini = "Selasa";
		break;

		case 'Wed':
			$hari_ini = "Rabu";
		break;

		case 'Thu':
			$hari_ini = "Kamis";
		break;

		case 'Fri':
			$hari_ini = "Jumat";
		break;

		case 'Sat':
			$hari_ini = "Sabtu";
		break;

		default:
			$hari_ini = "Tidak di ketahui";
		break;
	}

	return $hari_ini;

}

function getMonth($month){

    switch($month){
        case '01':
            $hari_ini = "Januari";
        break;

        case '02':
            $hari_ini = "Februari";
        break;

        case '03':
            $hari_ini = "Maret";
        break;

        case '04':
            $hari_ini = "April";
        break;

        case '05':
            $hari_ini = "Mei";
        break;

        case '06':
            $hari_ini = "Juni";
        break;

        case '07':
            $hari_ini = "Juli";
        break;

        case '08':
            $hari_ini = "Agustus";
        break;

        case '09':
            $hari_ini = "September";
        break;

        case '10':
            $hari_ini = "Oktober";
        break;

        case '11':
            $hari_ini = "November";
        break;

        case '12':
            $hari_ini = "Desember";
        break;

        default:
            $hari_ini = "Tidak di ketahui";
        break;
    }

    return $hari_ini;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
        </center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
        </center>
        <center>{{ @$success ? @$success : session('alert') }} </center>
    </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center>
            <h4><i class="icon fa fa-info"></i> Info!</h4>
        </center>
        <center>{{ @$info ? @$info : session('info') }} </center>
    </div>
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Print Catatan Pelanggaran
            <div class="margin-top-edit-print" style="float:right;">
                <a href="{{route('view.tutorial.print.pdf')}}" target="_blank" class="btn btn-info"><i class="fa fa-print btn-top-edit-print"></i> Tutorial Print PDF</a>
                <a href="{{route('view.catatan.pelanggaran.send.email',[ Request::segment(4), Session::get('admin_id') ])}}" class="btn btn-danger btn-top-edit-print"><i class="fa fa-paper-plane-o"></i> Kirim PDF ke EMAIL</a>
                <a href="{{route('view.catatan.pelanggaran')}}" class="btn btn-success btn-top-edit-print">Kembali</a>
            </div>
        </h1>
        <br>
        <br>
    </section>
    <div class="pad margin no-print margin-top-bg-edit-print">
        <div class="callout callout-success" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Catatan:</h4>
            Tolong Isi Form (input yang ada titik<sup>2</sup>) yang sudah ada, apabila ingin mengisi sendiri silahkan
            dikosong kan. Tolong juga Sesuaikan apabila anda ingin mencetak halaman ini pada page setelah anda menekan tombol <b style="background-color: white; color: black;padding:5px;margin:5px;">Cetak PDF</b>
        </div>
    </div>
    <form action="{{route('handle.pelanggaran.print', $data['catatan'][0]->id)}}" method="GET" target="_blank">
        @csrf
        @method('POST')
        <!-- Main content -->
        <section class="invoice" style="overflow: hidden;">
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <address style="font-size: 12px">
                        <center>
                            <strong>KOMANDO DAERAH MILITER XII/TANJUNGPURA</strong><br>
                            <strong>POLISI MILITER</strong><br>
                        </center>
                        <hr style="margin-top: 0px;margin-bottom: 0px;padding-bottom: 0px;">
                        <strong>UNTUK KEADILAN</strong><br>
                        <strong>NOMOR : <b>{{ $data['catatan'][0]->nomor }}</b></strong>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <table class="tabel-pekerjaan" style="float:right;">
                        <tbody>
                            <tr>
                                <td>ANGKATAN DARAT</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="1" type="checkbox" {{ $data['catatan'][0]->jenis_pekerjaan == 1 ? 'checked' : '' }}></td>
                            </tr>
                            <tr>
                                <td>ANGKATAN LAUT</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="2" type="checkbox" {{ $data['catatan'][0]->jenis_pekerjaan == 2 ? 'checked' : '' }}></td>
                            </tr>
                            <tr>
                                <td>ANGKATAN UDARA</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="3" type="checkbox" {{ $data['catatan'][0]->jenis_pekerjaan == 3 ? 'checked' : '' }}></td>
                            </tr>
                            <tr>
                                <td>PNS</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="4" type="checkbox" {{ $data['catatan'][0]->jenis_pekerjaan == 4 ? 'checked' : '' }}></td>
                            </tr>
                            <tr>
                                <td>SIPIL</td>
                                <td><input class="jenis-pekerjaan" name="jenis_pekerjaan[]" value="5" type="checkbox" {{ $data['catatan'][0]->jenis_pekerjaan == 5 ? 'checked' : '' }}></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12">
                    <center style="font-size: 12px;">
                        <strong>BERITA ACARA PELANGGARAN LALU LINTAS TERTENTU</strong><br>
                        <strong>(BALANG LALIN)</strong>
                    </center>
                </div>
            </div>
            <br>

            @if (@$data['member'])
            @foreach ($data['member'] as $membersKey => $valueMember)
            <div class="row"
                style="border: 1px solid black; border-bottom: 0px; margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding-top: 10px; text-indent: 4em;">
                        Pada hari ini <b>{{ getDay(date('D', strtotime($data['catatan'][0]->tgl))) }}</b>
                        ,tanggal <b>{{ date('d', strtotime($data['catatan'][0]->tgl)) }}</b>
                        ,bulan <b>{{ getMonth(date('m', strtotime($data['catatan'][0]->tgl))) }}</b>
                        ,tahun <b>{{ date('Y', strtotime($data['catatan'][0]->tgl)) }}</b>
                        ,sekira pukul <b>{{ date('H', strtotime($data['catatan'][0]->tgl)) }}</b> : <b>{{ date('i', strtotime($data['catatan'][0]->tgl)) }}</b>
                        di
                        jalan <b>{{ $data['catatan'][0]->jalan }}</b>,
                        kota <b>{{ $data['catatan'][0]->kota }}</b>,
                        petugas yang bertanda tangan di bawah ini, berdasarkan undang-undang RI Nomor 31
                        Tahun 1997 tentang Peradilan Militer dan Keputusan Panglima TNI Nomor Kep/650/VII/2011
                        tentang Penyerahan Perkara Pelanggaran Lalu Lintas, telah Melakukan pemeriksaan terhadap
                        sebuah kendaraan jenis <b>{{ $data['catatan'][0]->jenis_kendaraan }}</b>,
                        warna <b>{{ $data['catatan'][0]->warna }}</b>
                        Nomor Registrasi <b>{{ $data['catatan'][0]->nomor_registrasi }}</b>
                        Yang dikemudikan oleh seorang
                        <b>{{ $data['catatan'][0]->jenis_kelamin }}</b> :
                    </p>
                </div>
                <div class="col-xs-12 table-responsive">
                    <table class="table-data-diri">
                        <tbody>
                            <tr>
                                <td>Name </td>
                                <td class="table-indent">:</td>
                                <td class="text-indent-10 text-capitalize"> <input class="input-text-long-css-empty" name="nama_pengendara" type="text"
                                        value="{{$valueMember->name}}" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Pangkat/NRP </td>
                                <td class="table-indent">:</td>
                                <td class="text-indent-10 text-capitalize"> <input class="input-text-long-css-empty" name="pangkat_pengendara" type="text"
                                        value="{{$valueMember->pangkat}} / {{$valueMember->nrp}}" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan/Jabatan </td>
                                <td class="table-indent">:</td>
                                <td class="text-indent-10 text-capitalize"> <input class="input-text-long-css-empty" name="pekerjaan_atau_jabatan_pengendara"
                                        type="text" value="{{$valueMember->jabatan}}" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Kesatuan </td>
                                <td class="table-indent">:</td>
                                <td class="text-indent-10 text-capitalize"> <input class="input-text-long-css-empty" name="kesatuan_pengendara" type="text"
                                        value="{{$valueMember->kesatuan}}" style="text-align: left;" readonly></td>
                            </tr>
                            <tr>
                                <td>Alamat </td>
                                <td class="table-indent">:</td>
                                <td class="text-indent-10 text-capitalize"> <input class="input-text-long-css-empty" style="text-align: left;" name="alamat_pengendara" type="text"
                                    value="{{$valueMember->alamat_rumah}}"
                                    readonly></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12" style="padding-top:10px;">
                    <p class="text-indent text-opening-letter" style="text-align: justify;">
                        Yang diduga atau patut diduga telah melakukan pelanggaran lalu lintas yang diatur dalam
                        UU
                        Nomor 22 Tahun 2009 tentang lalu lintas dan Angkutan jalan :
                    </p>
                </div>
            </div>
            @endforeach
            @endif

            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding:0px;">
                    <table class="table-pelanggaran" border="1" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">Pasal Yang Dilanggar</th>
                                <th style="text-align: center;">Jenis Pelanggaran</th>
                                <th style="text-align: center;">(V)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>278</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor beroda empat atau lebih di jalan
                                    yang
                                    tidak
                                    dilengkapi dengan perlengkapan
                                    berupa ban cadangan, segitiga, dongkrak, pembuka roda dan peralatan PPPK.
                                </td>
                                <td>
                                    <input name="jenis_pelanggaran[]" type="checkbox" value="1" {{(in_array('1', $data['id_data_master']) ? 'checked' : '')}}/>
                                </td>
                            </tr>
                            <tr>
                                <td>280</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang tidak dipasangi
                                    tanda
                                    Nomor Ranmor
                                    yang di tetapkan</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="2" {{(in_array('2', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>281</td>
                                <td class="isi-pelanggaran">Mengemudi Ranmor di jalan yang tidak memiliki SIM.
                                </td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="3" {{(in_array('3', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>283</td>
                                <td class="isi-pelanggaran">Mengemudi Ranmor di jalan secara tidak wajar &
                                    melakukan
                                    kegiatan lain
                                    atau di pengaruhi oleh suatu
                                    keadaan yang mengakibatkan gangguan konsentrasi dalam mengemudi di jalan.
                                </td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="4" {{(in_array('4', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>284</td>
                                <td class="isi-pelanggaran">Mengemudi Ranmor dengan tidak mengutamakan
                                    keselamatan
                                    Pejalan kaki
                                    atau bersepeda.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="5" {{(in_array('5', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>285(1)</td>
                                <td class="isi-pelanggaran">Mengemudikan sepeda motor di jalan yang tidak
                                    memenuhi
                                    persyaratan
                                    teknis dan layak jalan yang
                                    meliputi kaca spion, klakson, lampu utama, lampu rem, lampu petunjuk arah,
                                    alat
                                    pemantul
                                    cahaya,
                                    alat pengukur kecepatan, knalpot dan kedalaman alur ban.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="6" {{(in_array('6', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>285(2)</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor beroda empat atau lebih di jalan
                                    tidak
                                    memenuhi
                                    persyaratan teknis yang meliputi
                                    kaca spion, klakson, lampu utama, lampu rem, lampu tanda batas dimensi badan
                                    kendaraan,
                                    lampu
                                    gandingan, lampu rem, lampu petunjuk arah, alat pemantul cahaya, alat
                                    pengukur
                                    kecepatan,
                                    kedalaman
                                    alur ban, kaca depan spakbor, bumper, penggandengan, penempelan atau
                                    penghapus kaca.
                                </td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="7" {{(in_array('7', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>287</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang melanggar aturan
                                    perintah
                                    atau
                                    larangan yang dinyatakan dengan
                                    Rambu lalu lintas / Marka jalan.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="8" {{(in_array('8', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>288(1)</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang tidak dilengkapi
                                    dengan
                                    STNK atau
                                    STCKB yang ditetapkan.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="9" {{(in_array('9', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>288(2)</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor di jalan yang tidak dapat
                                    menunjukan SIM
                                    yang sah.
                                </td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="10" {{(in_array('10', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>289</td>
                                <td class="isi-pelanggaran">Mengemudikan Ranmor atau Penumpang yang duduk di
                                    samping
                                    Pengemudi
                                    yang tidak mengenakan sabuk
                                    keselamatan.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="11" {{(in_array('11', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>291(1)</td>
                                <td class="isi-pelanggaran">Mengemudikan Sepeda Motor tidak mengenakan helm
                                    Standar
                                    Nasional
                                    Indonesia.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="12" {{(in_array('12', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>292(2)</td>
                                <td class="isi-pelanggaran">Mengemudikan sepeda Motor yang membiarkan
                                    penumpangnya tidak
                                    menggunakan Helm. </td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="13" {{(in_array('13', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>293(2)</td>
                                <td class="isi-pelanggaran">Mengemudikan Sepeda Motor di jaaln tanpa menyalakan
                                    lampu
                                    utama pada
                                    siang hari.</td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="14" {{(in_array('14', $data['id_data_master']) ? 'checked' : '')}}/></td>
                            </tr>
                            <tr>
                                <td>Lain-lain</td>
                                <td class="isi-pelanggaran"><textarea name="isi_pelanggaran_lain_lain" type="text"
                                        class="input-css-empty" style="width: 100%;text-align: left"></textarea></td>
                                <td><input name="jenis_pelanggaran[]" type="checkbox" value="lain" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                    <h4 style="margin-left: 0px; font-size: 12px; font-weight: bold;">Keterangan jenis pelanggaran</h4>
                    <hr style="margin-top: 15px;margin-bottom: 5px;">
                </div>
            </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;font-size: 12px;">
                    <p style="text-indent: 5em !important;"> Atas pelanggaran yang telah dilakukannya, kepada
                        Pelanggar
                        telah dibuatkan Berita Acara Pelanggaran Lalu Lintas dan kepadanya telah diberitahukan
                        untuk
                        mengahadap Hakim pada Peradilan Militer <b class="text-capitalize">{{ $data['catatan'][0]->peradilan_militer }}</b>
                        pada tanggal/waktu yang telah ditetapkan sesuai dengan surat panggilan.</p>
                    <p style="text-indent: 5em !important;"> Untuk kepentingan penyidik selanjutnya/menghentikan
                        pelanggaran yang telah terjadi, berdasarkan Pasal 260 UU No. 22 Tahun 2009, petugas
                        menahan/menyita untuk sementara : KENDARAAN/SIM/STNK/BENDA LAIN berupa : <b class="text-capitalize">{{ $data['catatan'][0]->sitaan }}</b>
                    </p>
                    <p style="text-indent: 5em !important;"> Demikian Berita Acara ini dibuat dengan sebenarnya,
                        kemudian untuk menguatkannya Tersangka, dan penyidik telah membubuhkan tanda tangan
                        seperti
                        tersebut dibawah ini :</p>
                </div>
            </div>
            <div class="row" style="margin-left: 10px; margin-right: 10px;font-size: 12px;">
                <div class="col-xs-4">
                    <h5><u><b>Tersangka</b></u></h5>
                    <h5>Tanda Tangan</h5>
                </div>
                <div class="col-xs-4">
                    <h5><u><b>Penyidik</b></u></h5>
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->penyidik_nama }}</b></td>
                            </tr>
                            <tr>
                                <td>Pangkat/NRP</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->penyidik_pangkat_nrp }}</b></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->penyidik_jabatan }}</b></td>
                            </tr>
                            <tr>
                                <td>Kesatuan</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->penyidik_kesatuan }}</b></td>
                            </tr>
                            <tr>
                                <td>Tanda Tangan</td>
                                <td class="table-indent"></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-4">
                    <h5><u><b>Disahkan</b></u></h5>
                    <table>
                        <tbody>
                            <tr>
                                <td>Di</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->disahkan_di }}</b></td>
                            </tr>
                            <tr>
                                <td>Pada Tanggal</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->disahkan_tgl }}</b></td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->disahkan_nama }}</b></td>
                            </tr>
                            <tr>
                                <td>Pangkat/NRP</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->disahkan_pangkat_nrp }}</b></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->disahkan_jabatan }}</b></td>
                            </tr>
                            <tr>
                                <td>Kesatuan</td>
                                <td class="table-indent"> : </td>
                                <td class="text-indent-10 text-capitalize"> <b>{{ $data['catatan'][0]->disahkan_kesatuan }}</b></td>
                            </tr>
                            <tr>
                                <td>Tanda Tangan</td>
                                <td class="table-indent"></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12"><br><br><br><br><br><br><br><br></div>
                <div class="col-xs-12" style="font-size: 12px;">
                    <p>Catatan :</p>
                    <ol>
                        <li>Terhadap pelanggaran yang terjadi, pada kolom yang tersedia diberikan tanda silang.
                        </li>
                        <li>Balang Lalin ini berlaku pula sebagai bukti penyitaan.</li>
                        <li>Lembar Merah Untuk Pelanggar.</li>
                        <li>Lembar Kuning Untuk Ankum.</li>
                        <li>Lembar Hijau Untuk Dilmil/Dilmilti.</li>
                        <li>Lembar Biru Untuk Otmil/Otmilti.</li>
                        <li>Lembar Putih Untuk Arsip Kesatuan.</li>
                    </ol>
                </div>
            </div>
            <br>
            <div class="row no-print">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> Cetak PDF</button>
                    <a href="{{route('view.catatan.pelanggaran.send.email',[ Request::segment(4), Session::get('admin_id') ])}}" class="btn btn-danger pull-right" style="margin-right: 5px;" name="cetak" value="1"><i class="fa fa-paper-plane-o"></i> Kirim PDF ke EMAIL</a>
                    <a href="{{route('view.pelanggaran.final.print')}}"  target="_blank" type="button" class="btn btn-success pull-right" style="margin-right: 5px;"><i class="fa fa-sticky-note-o"></i> Cetak Template PDF (Kosong Data)</a>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </form>
    <div class="clearfix"></div>
</div>
@endsection
@section('script')
<script>
    (function ($) {
        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop",
                function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
        };
    }(jQuery));

    //input logic attribut
    $(".input-num-only").inputFilter(function (value) {
        return /^\d*$/.test(value);
    });
    $('.input-text-css-empty').attr('maxlength', '10').attr('style', 'text-align:center; width: 80px;').attr(
        'autocomplete', 'off')
    $('.input-num-css-empty').attr('style', 'text-align:center; width: 200px;').attr('autocomplete', 'off')

    // logic date
    var date = new Date();
    var day = date.getDay();
    var month = date.getMonth();
    var year = date.getFullYear();
    if(date.getDate() < 10){
        tgl = "0"+ date.getDate();
    } else {
        tgl = date.getDate();
    }
    var tanggal = tgl + " - " + "0"+(month+1) + " - " + year
    var jam = date.getHours()
    var menit = date.getMinutes()
    if(jam < 10) {
        jam='0'+jam
    }
    var dayNames = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
    var monthNames = ["January", "February", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"]

    //auto fill input tanggal dan jam
    $('input[name=hari_ini_pembukaan]').val(dayNames[day])
    $('input[name=tanggal_pembukaan]').val(tanggal)
    $('input[name=bulan_pembukaan]').val(monthNames[month])
    $('input[name=tahun_pembukaan]').val(year)
    $('input[name=pukul_jam_kejadian_pembukaan]').val(jam)
    $('input[name=pukul_menit_kejadian_pembukaan]').val(menit)

    $('.jenis-pekerjaan').on("click", function() {
        $(".jenis-pekerjaan").prop('checked', false);
        $(this).prop('checked', true);
    })
</script>
@endsection
