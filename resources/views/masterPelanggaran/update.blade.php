@extends('template')

@section('title')
Update Master Pelanggaran
@endsection

@section('activeMasterPelanggaran')
active
@endsection

@section('pageName')
Update Master Pelanggaran
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')
<!-- Main content -->
<section class="content container-fluid">

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Error!<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (@$error || Session::has('alertErr'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <center><h4><i class="icon fa fa-ban"></i> Error!</h4></center>
        <center>{{ @$error ? @$error : session('alertErr') }} </center>
    </div>
    @endif

    @if (@$success || Session::has('alert'))
    <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-check"></i> Success!</h4></center>
            <center>{{ @$success ? @$success : session('alert') }} </center>
        </div>
    @endif

    @if (@$info || Session::has('info'))
    <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center><h4><i class="icon fa fa-info"></i> Info!</h4></center>
            <center>{{ @$info ? @$info : session('info') }} </center>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
        <h3 class="box-title">Update Master Pelanggaran</h3>
            <div style="float: right;">
                <a class="btn btn-success" href="{{route('view.master.pelanggaran')}}"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        @foreach ($data as $key => $value)
        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->
            <form class="form-horizontal" action="{{route('handle.master.pelanggaran.update.save')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="hidden" name="id" value="{{ $value->id }}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Pasal</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" name="pasal" id="pasal" value="{{ $value->pasal }}" placeholder="Pasal" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPangkat" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" style="width:100%; height:200px; padding:10px; font-size:1.5rem; text-align:center;" name="name" id="name">{{ $value->name }}</textarea>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- /.box-body -->
                <div class="box-footer" style="float:right;">
                    <a href="{{route('view.master.pelanggaran')}}" type="button" class="btn btn-primary">Kembali</a>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection
