<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/example/pdf/', [ 'uses' => 'ExampleController@index']);

Route::middleware(['CheckLoginInAuth'])->group(function () {
    Route::get('/', [ 'uses' => 'AuthController@login', 'as' => 'view.login.root']);
    Route::get('/login', [ 'uses' => 'AuthController@login', 'as' => 'view.login.get']);
    Route::post('/loginPost', [ 'uses' => 'AuthController@handleLoginPost', 'as' => 'handle.login.save']);
    Route::get('/register', [ 'uses' => 'AuthController@register', 'as' => 'view.register.get']);
    Route::post('/registerPost', [ 'uses' => 'AuthController@handleRegisterPost', 'as' => 'handle.register.save']);
});

Route::get('/logout', [ 'uses' => 'AuthController@handleLogout', 'as' => 'handle.logout']);

Route::middleware(['CheckLogin'])->group(function () {
    Route::get('/dashboard', [ 'uses' => 'CatatanPelanggaranController@viewCatatanPelanggaranGet', 'as' => 'view.dashboard']);

    Route::prefix('member')->group(function () {
        Route::get('/', [ 'uses' => 'MemberController@viewMemberGet', 'as' => 'view.member']);
        Route::get('/detail/{id}', [ 'uses' => 'MemberController@viewMemberDetail', 'as' => 'view.member.detail']);

        Route::prefix('detail')->group(function () {
            Route::get('/ajax/pelanggaran/{id}', [ 'uses' => 'MemberController@ajaxDetailMemberPelanggaran', 'as' => 'ajax.member.detail.pelanggaran']);
            Route::get('/ajax/{id}/{tipe}', [ 'uses' => 'MemberController@ajaxDetailMember', 'as' => 'ajax.member.detail']);

            Route::middleware(['CheckRole'])->group(function () {
                Route::get('/create/{id}/{tipe}', [ 'uses' => 'MemberController@viewDetailMemberCreate', 'as' => 'view.member.detail.create']);
                Route::post('/create', [ 'uses' => 'MemberController@handleDetailMemberCreate', 'as' => 'handle.member.detail.create.save']);
                Route::get('/update/{id_member}/{id_detail}/{tipe}', [ 'uses' => 'MemberController@viewDetailMemberUpdate', 'as' => 'view.member.detail.update']);
                Route::post('/update', [ 'uses' => 'MemberController@handleDetailMemberUpdate', 'as' => 'handle.member.detail.update.save']);
                Route::get('/delete/{id}/{id_member}', [ 'uses' => 'MemberController@handleDetailMemberDelete', 'as' => 'handle.member.detail.delete']);
            });
        });

        Route::middleware(['CheckRole'])->group(function () {
            Route::get('/create', [ 'uses' => 'MemberController@viewMemberCreate', 'as' => 'view.member.create']);
            Route::post('/create', [ 'uses' => 'MemberController@handleMemberCreate', 'as' => 'handle.member.create.save']);
            Route::get('/update/{id}', [ 'uses' => 'MemberController@viewMemberUpdate', 'as' => 'view.member.update']);
            Route::post('/update', [ 'uses' => 'MemberController@handleMemberUpdate', 'as' => 'handle.member.update.save']);
            Route::get('/delete/{id}', [ 'uses' => 'MemberController@handleMemberDelete', 'as' => 'handle.member.delete']);
        });
    });

    Route::prefix('admin')->group(function () {
        Route::get('/', [ 'uses' => 'AdminController@viewAdminGet', 'as' => 'view.admin']);
        Route::get('/detail/{id}', [ 'uses' => 'AdminController@viewAdminDetail', 'as' => 'view.admin.detail']);

        Route::middleware(['CheckRole'])->group(function () {
            Route::get('/create', [ 'uses' => 'AdminController@viewAdminCreate', 'as' => 'view.admin.create']);
            Route::post('/create', [ 'uses' => 'AdminController@handleAdminCreate', 'as' => 'handle.admin.create.save']);
            Route::get('/update/{id}', [ 'uses' => 'AdminController@viewAdminUpdate', 'as' => 'view.admin.update']);
            Route::post('/update', [ 'uses' => 'AdminController@handleAdminUpdate', 'as' => 'handle.admin.update.save']);
            Route::get('/delete/{id}', [ 'uses' => 'AdminController@handleAdminDelete', 'as' => 'handle.admin.delete']);
        });
    });

    Route::prefix('master/pelanggaran')->group(function () {
        Route::get('/', [ 'uses' => 'MasterPelanggaranController@viewMasterPelanggaranGet', 'as' => 'view.master.pelanggaran']);
        Route::get('/detail/{id}', [ 'uses' => 'MasterPelanggaranController@viewMasterPelanggaranDetail', 'as' => 'view.master.pelanggaran.detail']);

        Route::middleware(['CheckRole'])->group(function () {
            Route::get('/create', [ 'uses' => 'MasterPelanggaranController@viewMasterPelanggaranCreate', 'as' => 'view.master.pelanggaran.create']);
            Route::post('/create', [ 'uses' => 'MasterPelanggaranController@handleMasterPelanggaranCreate', 'as' => 'handle.master.pelanggaran.create.save']);
            Route::get('/update/{id}', [ 'uses' => 'MasterPelanggaranController@viewMasterPelanggaranUpdate', 'as' => 'view.master.pelanggaran.update']);
            Route::post('/update', [ 'uses' => 'MasterPelanggaranController@handleMasterPelanggaranUpdate', 'as' => 'handle.master.pelanggaran.update.save']);
            Route::get('/delete/{id}', [ 'uses' => 'MasterPelanggaranController@handleMasterPelanggaranDelete', 'as' => 'handle.master.pelanggaran.delete']);
        });
    });

    Route::prefix('catatan/pelanggaran')->group(function () {
        Route::get('/', [ 'uses' => 'CatatanPelanggaranController@viewCatatanPelanggaranGet', 'as' => 'view.catatan.pelanggaran']);
        Route::get('/detail/{id}', [ 'uses' => 'CatatanPelanggaranController@viewPelanggaranEditPDF', 'as' => 'view.catatan.pelanggaran.detail']);
        Route::get('/send/email/{id}/{adminId}', [ 'uses' => 'CatatanPelanggaranController@sendEmail', 'as' => 'view.catatan.pelanggaran.send.email']);

        Route::middleware(['CheckRole'])->group(function () {
            Route::get('/create', [ 'uses' => 'CatatanPelanggaranController@viewPelanggaranCreatePDF', 'as' => 'view.pelanggaran.create']);
            Route::post('/create', [ 'uses' => 'CatatanPelanggaranController@handlePelanggaranCreate', 'as' => 'handle.pelanggaran.create.save']);
            Route::get('/update/{id}/{id_member}', [ 'uses' => 'CatatanPelanggaranController@viewCatatanPelanggaranUpdate', 'as' => 'view.pelanggaran.update']);
            Route::post('/update', [ 'uses' => 'CatatanPelanggaranController@handleCatatanPelanggaranUpdate', 'as' => 'handle.pelanggaran.update.save']);
            Route::get('/delete/{id}/{id_member}', [ 'uses' => 'CatatanPelanggaranController@handleCatatanPelanggaranDelete', 'as' => 'handle.catatan.pelanggaran.delete']);
            Route::get('/tutorial/print', [ 'uses' => 'CatatanPelanggaranController@viewTutorialPrintPDF', 'as' => 'view.tutorial.print.pdf']);
            Route::get('/print/pdf/{id}', [ 'uses' => 'CatatanPelanggaranController@viewPelanggaranEditPDF', 'as' => 'view.pelanggaran.print']);
            Route::get('/handle/pdf{id}', [ 'uses' => 'CatatanPelanggaranController@handlePelanggaranCreatePDF', 'as' => 'handle.pelanggaran.print']);
            Route::get('/template/pdf', [ 'uses' => 'CatatanPelanggaranController@viewPelanggaranTemplatePDF', 'as' => 'view.pelanggaran.final.print']);
        });
    });
});
