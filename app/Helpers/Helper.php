<?php
namespace App\Helpers;

class Helper
{
    public function __construct()
    {
        //
    }

	/**
     * Function Mapping Data Request.
     *
     * @return void
     */
    public function mapping($request) {
        $arrAux = array();
        foreach($request->all() as $key => $value){
            $newKey = preg_replace("/(_?\d+)+$/","",$key);//this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    public function success($data, string $message=null, int $code=200)
    {
        return [
            'status' => true,
            'error' => false,
            'code' => $code,
            'data' => $data,
            'message' => $message,
        ];
    }

    public function error(string $message=null, int $code=400, $data=null)
    {
        return [
            'status' => false,
            'error' => true,
            'code' => $code,
            'data' => $data,
            'message' => $message,
        ];
    }

    public function getDay($hari){
 
        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;
     
            case 'Mon':			
                $hari_ini = "Senin";
            break;
     
            case 'Tue':
                $hari_ini = "Selasa";
            break;
     
            case 'Wed':
                $hari_ini = "Rabu";
            break;
     
            case 'Thu':
                $hari_ini = "Kamis";
            break;
     
            case 'Fri':
                $hari_ini = "Jumat";
            break;
     
            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";		
            break;

        }

        return $hari_ini;
    }

        public function getMonth($month){
 
            switch($month){
                case '01':
                    $hari_ini = "Januari";
                break;
         
                case '02':			
                    $hari_ini = "Februari";
                break;
         
                case '03':
                    $hari_ini = "Maret";
                break;
         
                case '04':
                    $hari_ini = "April";
                break;
         
                case '05':
                    $hari_ini = "Mei";
                break;
         
                case '06':
                    $hari_ini = "Juni";
                break;
         
                case '07':
                    $hari_ini = "Juli";
                break;

                case '08':
                    $hari_ini = "Agustus";
                break;

                case '09':
                    $hari_ini = "September";
                break;

                case '10':
                    $hari_ini = "Oktober";
                break;

                case '11':
                    $hari_ini = "November";
                break;

                case '12':
                    $hari_ini = "Desember";
                break;
                
                default:
                    $hari_ini = "Tidak di ketahui";		
                break;
            }
            
            return $hari_ini;
        }

}
