<?php
namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(@$request->session()->get('is_login') === false || $request->session()->has('is_login') === false ) {
            return redirect(route('view.login.get'))->with('alertErr', 'Silahkan Login Dahulu!');
        }
        return $next($request);
    }
}
