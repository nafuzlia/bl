<?php
namespace App\Http\Middleware;

use Closure;

class CheckLoginInAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('is_login') === true && @$request->session()->get('is_login') === true) {
            return redirect(route('view.dashboard'))->with('alert', 'Anda Sudah Login !');
        }
        return $next($request);
    }
}
