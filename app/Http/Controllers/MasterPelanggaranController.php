<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Helpers\Helper;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;

class MasterPelanggaranController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function viewMasterPelanggaranGet(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('master_pelanggaran')->latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = "<center><a href='".route('view.master.pelanggaran.detail', $row->id)."' data-toggle='tooltip' data-original-title='Detail' class='btn btn-success btn-sm'>Detail</a> <a href='".route('view.master.pelanggaran.update', $row->id)."' data-toggle='tooltip' data-original-title='Edit' class='btn btn-primary btn-sm'>Edit</a> <a onclick='deleteConfirm(".$row->id.")' data-toggle='tooltip' data-original-title='Delete' id='deleted_button' class='btn btn-danger btn-sm'>Delete</a> </center>";
                        return $btn;
                    })
                    ->addColumn('pasal', function($row){
                        $btn = "<a href='".route('view.master.pelanggaran.detail', $row->id)."'><b title='Klik Untuk Ke Detail!'>".$row->pasal."</b></a>";
                        return $btn;
                    })
                    ->rawColumns(['action', 'pasal'])
                    ->make(true);
        }
        return view('masterPelanggaran.get');
    }

    public function viewMasterPelanggaranDetail($id)
    {
        $data = $this->getOne($id);
        return view('masterPelanggaran.detail')->with('data', @$data['data']);
    }

    public function viewMasterPelanggaranCreate()
    {
        return view('masterPelanggaran.create');
    }

    public function handleMasterPelanggaranCreate(Request $request)
    {
        $request->validate([
            'pasal'  => 'required',
            'name'  => 'required',
        ]);

        $data = $this->add($request);
        if(@$data['status']) {
            return view('masterPelanggaran.create')->with('data', $data['data'])
                                            ->with('success', $data['message']);
        } else {
            return view('masterPelanggaran.create')->with('data', $data['data'])
                                            ->with('error', $data['message']);
        }
    }

    public function viewMasterPelanggaranUpdate($id)
    {
        $data = $this->getOne($id);
        if(@$data['status']) {
            return view('masterPelanggaran.update')->with('data', @$data['data']);
        } else {
            return view('masterPelanggaran.update')->with('error', @$data['message']);
        }
    }

    public function handleMasterPelanggaranUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $data = $this->update($request);
        if(@$data['status']) {
            return redirect(route('view.master.pelanggaran.update', $data['data']['id']))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.master.pelanggaran.update', $data['data']['id']))->with('alertErr', @$data['message']);
        }
    }

    public function handleMasterPelanggaranDelete($id)
    {
        $data = $this->delete($id);
        if(@$data['status']) {
            return redirect(route('view.master.pelanggaran'))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.master.pelanggaran'))->with('alertErr', @$data['message']);
        }
    }

    public function getOne($id)
    {
        $master_pelanggaran = DB::table('master_pelanggaran')->where('id', $id)->get()->toArray();
        switch (true) {
            case $master_pelanggaran == null|| $master_pelanggaran == '' || empty($master_pelanggaran) :
                return $this->helper->error('Data Tidak Ditemukan!', 404);
                break;
        }
        return $this->helper->success($master_pelanggaran, 'Data Ditemukan!');
    }

    public function add($request)
    {
        $pasal = $request->input('pasal');
        $name = $request->input('name');

        $insert = [
            'pasal' => $pasal,
            'name' => $name,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $members = DB::table('master_pelanggaran')->insert($insert);

        if($members) {
            $response = $this->helper->success($insert, 'Berhasil Membuat Data !');
        } else {
            $response = $this->helper->error('Gagal Membuat Data !', 404);
        }

        return $response;
    }

    public function update($request)
    {
        $id = $request->input('id');
        $pasal = $request->input('pasal');
        $name = $request->input('name');

        $update = [];
        if(!empty($pasal)) {
            $update['pasal'] = $pasal;
        }
        if(!empty($name)) {
            $update['name'] = $name;
        }
        if($id) {
            $update['updated_at'] = Carbon::now();
            $members = DB::table('master_pelanggaran')->where('id', (int)$id)->update($update);
            if(@$members) {
                $update['id'] = $id;
                $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');
            } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

    public function delete($id)
    {
        if ($id) {
            $members = DB::table('master_pelanggaran')->delete($id);
            if (@$members) $response = $this->helper->success(['id' => $id], 'Berhasil Menghapus Data Dengan ID '.$id.'!');
            else $response = $this->helper->error('Gagal Menghapus Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }
}
