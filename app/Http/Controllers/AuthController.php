<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function handleLoginPost(Request $request)
    {
        request()->validate([
            'email' => 'email',
            'password' => 'required',
        ]);
        $requestData = $this->helper->mapping($request);
        if(!empty(@$requestData)) {
            $data = DB::table('admins')->where('email', $requestData['email'])->first();
            if(@$data) {
                if(Hash::check($requestData['password'], $data->password)) {
                    Session::put('admin_id', $data->id);
                    Session::put('name', $data->name);
                    Session::put('email', $data->email);

                    if(@$data->role === 'Special') {
                        Session::put('is_admin', TRUE);
                    }

                    if(@$data->role === 'Regular') {
                        Session::put('is_admin', FALSE);
                    }

                    Session::put('is_login', TRUE);

                    return redirect(route('view.dashboard'));
                } else {
                    return redirect(route('view.login.get'))->with('alertErr','Password Salah !');
                }
            } else {
                return redirect(route('view.login.get'))->with('alertErr','Akun tidak Ditemukan!');
            }
        } else {
            return redirect(route('view.login.get'))->with('alertErr','Akun tidak Ditemukan!');
        }
    }

    public function handleRegisterPost(Request $request)
    {
        request()->validate([
            'name' => 'required|min:3|max:50',
            'email' => 'email',
            'password' => 'required|confirmed',
        ]);
        $requestData = $this->helper->mapping($request);
        $insert = [
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'password' => Hash::make($requestData['password']),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
        if(!empty(@$requestData)) {
            $checkEmail = DB::table('admins')->where('email', $requestData['email'])->first();
            if(@$checkEmail){
                return redirect(route('view.register.get'))->with('alertErr','Email Sudah Digunakan !');
            } else {
                $data = DB::table('admins')->insertGetId((array)$insert);
                if(@$data) {
                    Session::put('name', $requestData['name']);
                    Session::put('is_login', TRUE);
                    return redirect(route('view.dashboard'));
                } else {
                    return redirect(route('view.register.get'))->with('alertErr','Akun tidak Ditemukan!');
                }
            }
        } else {
            return redirect(route('view.register.get'));
        }
    }

    public function handleLogout()
    {
        Session::put('admin_id', NULL);
        Session::put('name', NULL);
        Session::put('is_admin', NULL);
        Session::put('is_login', FALSE);
        return redirect(route('view.login.get'))->with('alert','Anda sudah logout !');
    }
}
