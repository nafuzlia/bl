<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

use App\Helpers\Helper;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;

class MemberController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function viewMemberGet(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('members')->latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = "<center><a href='".route('view.member.detail', $row->id)."' data-toggle='tooltip' data-original-title='Detail' class='btn btn-success btn-sm'>Detail</a> <a href='".route('view.member.update', $row->id)."' data-toggle='tooltip' data-original-title='Edit' class='btn btn-primary btn-sm'>Edit</a> <a onclick='deleteConfirm(".$row->id.")' data-toggle='tooltip' data-original-title='Delete' id='deleted_button' class='btn btn-danger btn-sm'>Delete</a> </center>";
                        return $btn;
                    })
                    ->addColumn('foto', function($row){
                        $btn = "<center><img src=".($row->foto ? $row->foto : asset('default/default.png'))." height='50' width='50' style='border-radius:100%;'></center>";
                        return $btn;
                    })
                    ->addColumn('nrp', function($row){
                        $btn = "<a href='".route('view.member.detail', $row->id)."'><b title='Klik Untuk Ke Detail!'>".$row->nrp."</b></a>";
                        return $btn;
                    })
                    ->rawColumns(['nrp','action','foto'])
                    ->make(true);
        }
        return view('member.get');
    }

    public function viewMemberDetail($id)
    {
        $data = $this->getOne($id);
        return view('member.detail')->with('data', @$data['data']);
    }

    public function ajaxDetailMemberPelanggaran(Request $request, int $id=0)
    {
        if ($request->ajax()) {
            $data = DB::table('catatan_pelanggaran')
            ->select('catatan_pelanggaran.id as id_pelanggaran','catatan_pelanggaran.id_member' , 'catatan_pelanggaran.tgl as tanggal', 'catatan_pelanggaran.status')
            ->where(['catatan_pelanggaran.id_member' => $id ])
            ->latest()
            ->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('tanggal', function($row){
                        return date('d F Y', strtotime($row->tanggal));
                    })
                    ->addColumn('status', function($row){
                        if($row->status == 'Melanggar') {
                            $status = "<center><a href='".route('view.catatan.pelanggaran.detail', $row->id_pelanggaran)."' class='alstatus melanggar' readonly>{$row->status}</a></center>";
                        } else if($row->status == 'Lengkap') {
                            $status = "<center><a href='".route('view.catatan.pelanggaran.detail', $row->id_pelanggaran)."' class='alstatus lengkap' readonly>{$row->status}</a></center>";
                        }
                        return $status;
                    })
                    ->addColumn('action', function($row){
                        $action = "<center><a href='".route('view.catatan.pelanggaran.detail', [ $row->id_pelanggaran ])."' data-toggle='tooltip' data-original-title='Detail' class='btn btn-success btn-sm'>Detail</a> <a href='".route('view.pelanggaran.update', [ $row->id_pelanggaran, $row->id_member ])."' data-toggle='tooltip' data-original-title='Edit' class='btn btn-primary btn-sm'>Edit</a> <a onclick='deletePelanggaranConfirm(".$row->id_pelanggaran.",".$row->id_member.")' data-toggle='tooltip' data-original-title='Delete' id='deleted_button' class='btn btn-danger btn-sm'>Delete</a> </center>";
                        return $action;
                    })
                    ->rawColumns(['action', 'tanggal', 'status'])
                    ->make(true);
        }
    }

    public function ajaxDetailMember(Request $request, int $id=0, string $tipe="")
    {
        if ($request->ajax()) {
            $data = DB::table('detail_member')
            ->join('members', 'detail_member.id_member', '=', 'members.id')
            ->select('members.id as id_member', 'members.name', 'detail_member.id as id_detail_member', 'detail_member.riwayat', 'detail_member.tahun', 'detail_member.tipe', 'detail_member.created_at')
            ->where(['id_member' => $id, 'detail_member.tipe' => $tipe])
            ->latest()
            ->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $action = "<center><a href='".route('view.member.detail.update',[ $row->id_member, $row->id_detail_member, $row->tipe ])."' data-toggle='tooltip' data-original-title='Edit' class='btn btn-primary btn-sm'>Edit</a> <a onclick='deleteConfirm(".$row->id_detail_member.','.$row->id_member.")' data-toggle='tooltip' data-original-title='Delete' id='deleted_button' class='btn btn-danger btn-sm'>Delete</a> </center>";
                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    public function viewMemberCreate()
    {
        return view('member.create');
    }

    public function viewDetailMemberCreate(int $id=0, string $tipe="")
    {
        $data = [
            'id' => $id,
            'tipe' => @$tipe,
        ];
        if($tipe === 'UMUM') {
            $data['header'] = "Sekolah Umum";
        }
        if($tipe === 'MILITER') {
            $data['header'] = "Sekolah Militer";
        }
        if($tipe === 'JABATAN') {
            $data['header'] = "Jabatan";
        }
        return view('member.detail.create')->with('data', @$data);
    }

    public function handleMemberCreate(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'pangkat'  => 'required',
            'jabatan'  => 'required',
            'nrp'  => 'required',
            'kesatuan'  => 'required',
            'tinggi'  => 'required',
            'berat'  => 'required',
            'tempat_lahir'  => 'required',
            'tanggal_lahir'  => 'required',
            'tanggal_lahir'  => 'required',
            'agama'  => 'required',
            'alamat_kesatuan'  => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $data = $this->add($request);
        if(@$data['status']) {
            return view('member.create')->with('data', $data['data'])
                                            ->with('success', $data['message']);
        } else {
            return view('member.create')->with('data', $data['data'])
                                            ->with('error', $data['message']);
        }
    }

    public function handleDetailMemberCreate(Request $request)
    {
        $request->validate([
            'id'  => 'required',
            'tipe'  => 'required',
            'riwayat'  => 'required',
            'tahun_mulai'  => 'required',
        ]);

        $id = $request->input('id');
        $tipe = $request->input('tipe');

        $data = $this->addDetailMembers($request);

        $viewData = [
            'id' => $id,
            'tipe' => $tipe,
        ];

        if($tipe === 'UMUM') {
            $viewData['header'] = "Sekolah Umum";
        }
        if($tipe === 'MILITER') {
            $viewData['header'] = "Sekolah Militer";
        }
        if($tipe === 'JABATAN') {
            $viewData['header'] = "Jabatan";
        }
        if(@$data['status']) {
            return view('member.detail.create')->with('data', $viewData)
                                            ->with('success', $data['message']);
        } else {
            return view('member.detail.create')->with('data', $viewData)
                                            ->with('error', $data['message']);
        }
    }

    public function viewMemberUpdate($id)
    {
        $data = $this->getOne($id);
        if(@$data['status']) {
            return view('member.update')->with('data', @$data['data']);
        } else {
            return view('member.update')->with('error', @$data['message']);
        }
    }

    public function viewDetailMemberUpdate(int $id_member=0, int $id_detail=0, string $tipe="")
    {
        $data = $this->getOneDetail($id_detail);
        $viewData = [
            'id' => $id_member,
            'tipe' => $tipe,
        ];

        if($tipe === 'UMUM') {
            $viewData['header'] = "Sekolah Umum";
        }
        if($tipe === 'MILITER') {
            $viewData['header'] = "Sekolah Militer";
        }
        if($tipe === 'JABATAN') {
            $viewData['header'] = "Jabatan";
        }
        $data['view_data'] = $viewData;
        $tahunArr = [];
        foreach ($data['data'] as $value) {
            $tahunArr2 = explode('-', $value->tahun);
            array_push($tahunArr, $tahunArr2);
        }
        $data['tahun_data'] = $tahunArr;
        if(@$data['status']) {
            return view('member.detail.update')->with('data', @$data);
        } else {
            return view('member.detail.update')->with('error', @$data['message']);
        }
    }

    public function handleMemberUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);

        $data = $this->update($request);
        if(@$data['status']) {
            return redirect(route('view.member.update', $data['data']['id']))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member.update', $data['data']['id']))->with('alertErr', @$data['message']);
        }
    }

    public function handleDetailMemberUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'tipe' => 'required',
        ]);
        $id = $request->input('id');

        $id_member = $request->input('id_member');
        $tipe = $request->input('tipe');

        $data = $this->updateDetailMember($request);

        $viewData = [
            'id' => $id_member,
            'tipe' => $tipe,
        ];

        if($tipe === 'UMUM') {
            $viewData['header'] = "Sekolah Umum";
        }
        if($tipe === 'MILITER') {
            $viewData['header'] = "Sekolah Militer";
        }
        if($tipe === 'JABATAN') {
            $viewData['header'] = "Jabatan";
        }
        $data['view_data'] = $viewData;
        if(@$data['status']) {
            return redirect(route('view.member.detail.update', [$id_member, $id, $tipe]))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member.detail.update', [$id_member, $id, $tipe]))->with('alertErr', @$data['message']);
        }
    }

    public function handleMemberDelete($id)
    {
        $data = $this->delete($id);
        if(@$data['status']) {
            return redirect(route('view.member'))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member'))->with('alertErr', @$data['message']);
        }
    }

    public function handleDetailMemberDelete($id, $id_member)
    {
        $data = $this->deleteDetail($id);
        if(@$data['status']) {
            return redirect(route('view.member.detail', @$id_member))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member.detail', @$id_member))->with('alertErr', @$data['message']);
        }
    }

    public function handleDetailMemberPelanggaranDelete($id_member, $id_pelanggaran)
    {
        $data = $this->deletePelanggaran($id_pelanggaran);
        if(@$data['status']) {
            return redirect(route('view.member.detail', @$id_member))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member.detail', @$id_member))->with('alertErr', @$data['message']);
        }
    }

    public function getOne($id)
    {
        $members = DB::table('members')->where('id', $id)->get()->toArray();
        switch (true) {
            case $members == null|| $members == '' || empty($members) :
                return $this->helper->error('Data Tidak Ditemukan!', 404);
                break;
        }
        return $this->helper->success($members, 'Data Ditemukan!');
    }

    public function getOneDetail($id)
    {
        $DetailMember = DB::table('detail_member')->where('id', @$id)->get()->toArray();
        switch (true) {
            case $DetailMember == null|| $DetailMember == '' || empty($DetailMember) :
                return $this->helper->error('Data Tidak Ditemukan!', 404);
                break;
        }
        return $this->helper->success($DetailMember, 'Data Ditemukan!');
    }

    public function add($request)
    {
        $name = $request->input('name');
        $pangkat = $request->input('pangkat');
        $jabatan = $request->input('jabatan');
        $nrp = $request->input('nrp');
        $kesatuan = $request->input('kesatuan');
        $tinggi = $request->input('tinggi');
        $berat = $request->input('berat');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $agama = $request->input('agama');
        $alamat_rumah = $request->input('alamat_rumah');
        $alamat_kesatuan = $request->input('alamat_kesatuan');

        $fotoFile = $request->file('foto');
        $resizeImage = Image::make($fotoFile)->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 70)->encode('data-url');

        $fotoDataUrl = ''.$resizeImage; //ubah ke string

        $insert = [
            'name' => $name,
            'pangkat' => $pangkat,
            'jabatan' => $jabatan,
            'nrp' => $nrp,
            'kesatuan' => $kesatuan,
            'tinggi' => $tinggi,
            'berat' => $berat,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => date('Y-m-d', strtotime($tanggal_lahir)),
            'agama' => $agama,
            'alamat_rumah' => $alamat_rumah,
            'alamat_kesatuan' => $alamat_kesatuan,
            'foto' => $fotoDataUrl,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
        $members = DB::table('members')->insertGetId($insert);
        if($members) {
            $response = $this->helper->success($insert, 'Berhasil Membuat Data !');
        } else {
            $response = $this->helper->error('Gagal Membuat Data !', 404);
        }

        return $response;
    }

    public function addDetailMembers($request)
    {
        $id = $request->input('id');
        $riwayat = $request->input('riwayat');
        $tahunMulai = $request->input('tahun_mulai');
        $tahunSampai = $request->input('tahun_sampai');
        $tipe = $request->input('tipe');
        $data = [
            'id_member' => $id,
            'riwayat' => $riwayat,
            'tipe' => $tipe,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
        if($tahunMulai === $tahunSampai) {
            $data['tahun'] = $tahunMulai;
        }
        if(empty($tahunSampai)) {
            $data['tahun'] = $tahunMulai;
        }
        if(!empty($tahunMulai) && !empty($tahunSampai)) {
            $data['tahun'] = $tahunMulai.' - '.$tahunSampai;
        }
        $detailMembers = DB::table('detail_member')->insertGetId($data);

        if(@$detailMembers) {
            $response = $this->helper->success($detailMembers, 'Berhasil Membuat Data !');
        } else {
            $response = $this->helper->error('Gagal Membuat Data !', 404);
        }

        return $response;
    }

    public function updateDetailMember($request)
    {
        $id = $request->input('id');
        $riwayat = $request->input('riwayat');
        $tahunMulai = $request->input('tahun_mulai');
        $tahunSampai = $request->input('tahun_sampai');

        if($tahunMulai === $tahunSampai) {
            $tahun = $tahunMulai;
        }
        if(empty($tahunSampai)) {
            $tahun = $tahunMulai;
        }
        if(!empty($tahunMulai) && !empty($tahunSampai)) {
            $tahun = $tahunMulai.' - '.$tahunSampai;
        }

        $update = [];
        if(!empty($riwayat)) {
            $update['riwayat'] = $riwayat;
        }

        if(!empty($tahun)) {
            $update['tahun'] = $tahun;
        }

        if($id) {
            $update['updated_at'] = Carbon::now();
            $detailMembers = DB::table('detail_member')->where('id', (int)$id)->update($update);
            if(@$detailMembers) {
                $update['id'] = $id;
                $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');
            } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

    public function update($request)
    {
        $id = $request->input('id');
        $name = $request->input('name');
        $pangkat = $request->input('pangkat');
        $jabatan = $request->input('jabatan');
        $nrp = $request->input('nrp');
        $kesatuan = $request->input('kesatuan');
        $tinggi = $request->input('tinggi');
        $berat = $request->input('berat');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $agama = $request->input('agama');
        $alamat_rumah = $request->input('alamat_rumah');
        $alamat_kesatuan = $request->input('alamat_kesatuan');

        $fotoFile = $request->file('foto');
        if(@$fotoFile) {
            $resizeImage = Image::make($fotoFile)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('data-url');
            $fotoDataUrl = ''.$resizeImage;
        }

        $update = [];
        if(!empty($name)) {
            $update['name'] = $name;
        }
        if(!empty($pangkat)) {
            $update['pangkat'] = $pangkat;
        }
        if(!empty($jabatan)) {
            $update['jabatan'] = $jabatan;
        }
        if(!empty($nrp)) {
            $update['nrp'] = $nrp;
        }
        if(!empty($kesatuan)) {
            $update['kesatuan'] = $kesatuan;
        }
        if(!empty($alamat_rumah)) {
            $update['tinggi'] = $tinggi;
        }
        if(!empty($berat)) {
            $update['berat'] = $berat;
        }
        if(!empty($tempat_lahir)) {
            $update['tempat_lahir'] = $tempat_lahir;
        }
        if(!empty($tanggal_lahir)) {
            $update['tanggal_lahir'] = date('Y-m-d', strtotime($tanggal_lahir));
        }
        if(!empty($agama)) {
            $update['agama'] = $agama;
        }
        if(!empty($alamat_rumah)) {
            $update['alamat_rumah'] = $alamat_rumah;
        }
        if(!empty($alamat_kesatuan)) {
            $update['alamat_kesatuan'] = $alamat_kesatuan;
        }
        if(!empty(@$fotoDataUrl)) {
            $update['foto'] = $fotoDataUrl;
        }

        if($id) {
            $update['updated_at'] = Carbon::now();
            $members = DB::table('members')->where('id', (int)$id)->update($update);
            if(@$members) {
                $update['id'] = $id;
                $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');
            } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

    public function delete($id)
    {
        if ($id) {
            $members = DB::table('members')->delete($id);
            if (@$members) $response = $this->helper->success(['id' => $id], 'Berhasil Menghapus Data Dengan ID '.$id.'!');
            else $response = $this->helper->error('Gagal Menghapus Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

    public function deleteDetail($id)
    {
        if ($id) {
            $members = DB::table('detail_member')->delete($id);
            if (@$members) $response = $this->helper->success(['id' => $id], 'Berhasil Menghapus Data Dengan ID Detail Riwayat '.$id.'!');
            else $response = $this->helper->error('Gagal Menghapus Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }
}
