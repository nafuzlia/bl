<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use PDF;

use App\Helpers\Helper;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;

class CatatanPelanggaranController extends Controller
{
    private $detail;
    private $member;

    private $helper;

    public function __construct()
    {
        $this->detail = new DetailPelanggaranController;
        $this->member = new MemberController;

        $this->helper = new Helper;
    }

    public function viewTutorialPrintPDF()
    {
        return view('catatanPelanggaran.tutorial_print_pdf');
    }

    public function viewPelanggaranDetailPDF()
    {
        return view('catatanPelanggaran.print_pdf');
    }

    public function viewPelanggaranTemplatePDF()
    {
        return view('catatanPelanggaran.print_template_pdf');
    }

    public function viewCatatanPelanggaranGet(Request $request)
    {
        if ($request->ajax()) {
            if(!empty($request->from_date)) {
                $from_date = date('Y-m-d', strtotime($request->from_date)).' 00:00:00';
                $to_date = date('Y-m-d', strtotime($request->to_date)).' 23:59:59';

                $data = DB::table('catatan_pelanggaran')
                ->join('members', 'catatan_pelanggaran.id_member', '=', 'members.id')
                ->select('catatan_pelanggaran.id', 'catatan_pelanggaran.status', 'members.id as id_member', 'members.name', 'members.foto', 'members.nrp', 'members.pangkat', 'members.kesatuan','members.jabatan', 'catatan_pelanggaran.tgl', 'catatan_pelanggaran.created_at', 'catatan_pelanggaran.updated_at')
                ->whereBetween('catatan_pelanggaran.tgl', array($from_date, $to_date))
                ->latest()
                ->get();
            } else {
                $data = DB::table('catatan_pelanggaran')
                ->join('members', 'catatan_pelanggaran.id_member', '=', 'members.id')
                ->select('catatan_pelanggaran.id', 'catatan_pelanggaran.status', 'members.id as id_member', 'members.name', 'members.foto', 'members.nrp', 'members.pangkat', 'members.kesatuan','members.jabatan', 'catatan_pelanggaran.tgl', 'catatan_pelanggaran.created_at', 'catatan_pelanggaran.updated_at')
                ->latest()
                ->get();
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('foto', function($row){
                        $btn = "<center><img class='rounded' src=".($row->foto ? $row->foto : asset('default/default.png'))." height='50' width='50' ></center>";
                        return $btn;
                    })
                    ->addColumn('nrp', function($row){
                        $btn = "<a style='color:#3c8dbc;' href='".route('view.catatan.pelanggaran.detail', $row->id)."'><b>".$row->nrp."</b></a>";
                        return $btn;
                    })
                    ->addColumn('name', function($row){
                        $name = "<center><div style='text-align:left;color:#000;font-weight:bold'>{$row->name}</div><div style='text-align:left;color:#AFAFAF'>{$row->pangkat}</div></center>";
                        return $name;
                    })
                    ->addColumn('pangkat', function($row){
                        $name = "<div style='text-align:left;color:#AFAFAF;max-width: 200px !important;text-overflow: ellipsis !important;overflow: hidden;white-space: nowrap;'>{$row->jabatan}</div><div style='text-align:left;color:#AFAFAF;max-width: 200px !important;text-overflow: ellipsis !important;overflow: hidden;white-space: nowrap;'>{$row->kesatuan}</div></<div>";
                        return $name;
                    })
                    ->addColumn('tanggal', function($row){
                        $name = "<center>".date('d F Y', strtotime($row->tgl))."</center>";
                        return $name;
                    })
                    ->addColumn('status', function($row){
                        if($row->status == 'Melanggar') {
                            $status = "<center><a href='".route('view.catatan.pelanggaran.detail', $row->id)."' class='alstatus melanggar' readonly>{$row->status}</a></center>";
                        } else if($row->status == 'Lengkap') {
                            $status = "<center><a href='".route('view.catatan.pelanggaran.detail', $row->id)."' class='alstatus lengkap' readonly>{$row->status}</a></center>";
                        }
                        return $status;
                    })
                    ->rawColumns(['nrp', 'tanggal', 'foto','name', 'pangkat', 'status'])
                    ->make(true);
        }
        return view('catatanPelanggaran.get');
    }

    public function viewPelanggaranCreate()
    {
        $members = DB::table('members')->get();
        $master_pelanggaran = DB::table('master_pelanggaran')->get();
        $data = [
            'member' => $members,
            'masterPelanggaran' => $master_pelanggaran
        ];

        return view('catatanPelanggaran.create')->with('data', $data);
    }

    public function handlePelanggaranCreate(Request $request)
    {

        $cetak = $request->input('cetak');
        $request->validate([
            'id_member'  => 'required'
        ]);

        $id_master_pelanggaran = $request->input('jenis_pelanggaran');
        $status = 'Lengkap';
        if($id_master_pelanggaran){
            $status = 'Melanggar';
        }
        $catatan = $this->add($request, $status);
        if(@$catatan['status']) {
            $detailData = [
                'id_master_pelanggaran' => (string)json_encode($id_master_pelanggaran),
                'id_catatan_pelanggaran' => $catatan['data']['id']
            ];
            $detail = $this->detail->add($detailData);
            if(@$detail) {
                if($cetak){
                    return redirect(route('handle.pelanggaran.print', $catatan['data']['id']))->with('alertErr', @$catatan['message']);
                }
                return redirect(route('view.pelanggaran.create'))->with('alert', @$detail['message']);
            } else {
                if($cetak){
                    return redirect(route('handle.pelanggaran.print', $catatan['data']['id']))->with('alertErr', @$catatan['message']);
                }
                return redirect(route('view.pelanggaran.create'))->with('alertErr', @$detail['message']);
            }
        } else {
            if($cetak){
                return redirect(route('handle.pelanggaran.print', $catatan['data']['id']))->with('alertErr', @$catatan['message']);
            }
            return redirect(route('view.pelanggaran.create'))->with('alertErr', @$catatan['message']);
        }
    }

    public function viewPelanggaranCreatePDF()
    {
        $master_pelanggaran = DB::table('master_pelanggaran')->get();
        $members = DB::table('members')->get();
        $mainData = [
            'member' => $members,
            'master_pelanggaran' => $master_pelanggaran
        ];
        return view('catatanPelanggaran.print')->with('data', $mainData);
    }

    public function viewPelanggaranEditPDF($id)
    {
        $data = $this->getOne($id);
        $id_member=0;
        foreach($data['data']['catatan'] as $key => $value) {
            $id_member+=$value->id_member;
        }
        $members = $this->member->getOne($id_member);
        $mainData = [
            'member' => $members['data'],
            'catatan' => $data['data']['catatan'],
            'data_master' => $data['data']['data_master'],
            'id_data_master' => $data['data']['id_data_master']
        ];

        return view('catatanPelanggaran.edit_print')->with('data', $mainData);
    }

    public function sendEmail(int $id = 0, int $adminId = 0)
    {
        if(!empty(env('MAIL_USERNAME')) && !empty(env('MAIL_PASSWORD'))) {
            $data = $this->getOne($id);
            $id_member=0;
            foreach($data['data']['catatan'] as $key => $value) {
                $id_member+=$value->id_member;
            }
            $members = $this->member->getOne($id_member);

            $mainData = [
                'member' => $members['data'],
                'catatan' => $data['data']['catatan'],
                'data_master' => $data['data']['data_master'],
                'id_data_master' => $data['data']['id_data_master']
            ];

            $admin = DB::table('admins')->where('id', $adminId)->get();
            $email = $admin[0]->email;

            $memberName = '';
            foreach ($members['data'] as $membersKey => $valueMember) {
                $memberName = ''.$valueMember->name;
            }

            $dataEmail['email'] = $email.'';
            $dataEmail['member'] = $members['data'];
            $dataEmail['client_name'] = $memberName;
            $dataEmail['subject'] = 'Catatan Pelanggaran '.$memberName;

            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            $pdf = PDF::loadView('pdf_new', ['data' => @$mainData]);

            Mail::send('mail', $dataEmail, function($message) use ( $dataEmail, $pdf ) {
                $message->to($dataEmail["email"], $dataEmail["client_name"])
                    ->from($dataEmail["email"], 'Admin Balang Lalin Web')
                    ->subject($dataEmail["subject"])
                    ->attachData($pdf->output(), "Catatan_Pelanggaran_".$dataEmail["client_name"].".pdf");
            });

            if(Mail::failures()) {
                return redirect(route('view.catatan.pelanggaran.detail', [ $id ]))->with('alertErr', 'Email Gagal Terkirim!');
            } else {
                return redirect(route('view.catatan.pelanggaran.detail', [ $id ]))->with('alert', 'Email Berhasil Terkirim!');
            }
        } else {
            return redirect(route('view.catatan.pelanggaran.detail', [ $id ]))->with('alertErr', 'Konfigurasi Email Belum Diatur!');
        }
    }

    public function handlePelanggaranCreatePDF($id)
    {
        $data = $this->getOne($id);
        $id_member=0;
        foreach($data['data']['catatan'] as $key => $value) {
            $id_member+=$value->id_member;
        }
        $members = $this->member->getOne($id_member);
        $mainData = [
            'member' => $members['data'],
            'catatan' => $data['data']['catatan'],
            'data_master' => $data['data']['data_master'],
            'id_data_master' => $data['data']['id_data_master']
        ];
        return view('catatanPelanggaran.print_pdf')->with('data', $mainData);
    }

    public function viewCatatanPelanggaranDetail($id)
    {
        $data = $this->getOne($id);
        return view('catatanPelanggaran.detail')->with('data', @$data['data']);
    }

    public function viewCatatanPelanggaranUpdate($id, $id_member)
    {
        $members = DB::table('members')->get();
        $master_pelanggaran = DB::table('master_pelanggaran')->get();

        $getDataMember = $this->getOne($id);

        $dataMaster = [
            'member' => $members,
            'masterPelanggaran' => $master_pelanggaran
        ];

        $data = [
            'data_master' => @$dataMaster,
            'data_member' => @$getDataMember['data'],
            'id_member' => @$id_member
        ];

        return view('catatanPelanggaran.update')->with('data', @$data);
    }

    public function handleCatatanPelanggaranUpdate(Request $request)
    {
        $id_member = $request->input('id_member');
        $data = $this->update($request);
        if(@$data['status']) {
            return redirect(route('view.member.detail', [$id_member]))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member.detail', [$id_member]))->with('alertErr', @$data['message']);
        }
    }

    public function handleCatatanPelanggaranDelete($id, $id_member)
    {
        $data = $this->delete($id);
        if(@$data['status']) {
            return redirect(route('view.member.detail', [$id_member]))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.member.detail', [$id_member]))->with('alertErr', @$data['message']);
        }
    }

    public function getOne($id)
    {
        $catatan_pelanggaran = DB::table('catatan_pelanggaran')
                                    ->join('members', 'catatan_pelanggaran.id_member', '=', 'members.id')
                                    ->join('detail_pelanggaran', 'detail_pelanggaran.id_catatan_pelanggaran', '=', 'catatan_pelanggaran.id')
                                    ->select('catatan_pelanggaran.*','members.name', 'detail_pelanggaran.id as id_detail', 'detail_pelanggaran.id_master_pelanggaran as id_master')
                                    ->where('catatan_pelanggaran.id', $id)
                                    ->get()
                                    ->toArray();
        $ids = [];
        foreach ($catatan_pelanggaran as $key => $value) {
            if($value->id_master !== "null") {
                foreach (json_decode(@$value->id_master) as $id_master ) {
                    array_push($ids, $id_master);
                }
            }
        }

        $data_master = DB::table('master_pelanggaran')->whereIn('id', $ids)->get()->toArray();
        switch (true) {
            case $catatan_pelanggaran == null|| $catatan_pelanggaran == '' || empty($catatan_pelanggaran) :
                return $this->helper->error('Data Tidak Ditemukan!', 404);
                break;
        }
        $data = [
            'catatan' => $catatan_pelanggaran,
            'data_master' =>$data_master,
            'id_data_master' => $ids
        ];
        return $this->helper->success($data, 'Data Ditemukan!');
    }

    public function add($request, $status = 'Lengkap')
    {
        $id_member = $request->input('member_id');
        $tgl = $request->input('tanggal_pembukaan')."-".$request->input('bulan_pembukaan')."-".$request->input('tahun_pembukaan')." ".$request->input('pukul_jam_kejadian_pembukaan').":".$request->input('pukul_menit_kejadian_pembukaan');
        $jalan = $request->input('alamat_jalan');
        $kota = $request->input('alamat_kota');
        $insert = [
            'nomor' => $request->input('untuk_keadilan_nomor'),
            'id_member' => $id_member,
            'jenis_pekerjaan' => $request->input('jenis_pekerjaan')[0],
            'tgl' => date('Y-m-d H:i', strtotime($tgl)),
            'jalan' => $request->input('alamat_jalan'),
            'kota' => $request->input('alamat_kota'),
            'jenis_kendaraan' => $request->input('jenis_kendaraan'),
            'warna' => $request->input('warna_kendaraan'),
            'nomor_registrasi' => $request->input('nomor_kendaraan'),
            'jenis_kelamin' => $request->input('kelamin_pengendara'),
            'peradilan_militer' => $request->input('peradilan_militer'),
            'sitaan' => $request->input('menahan_atau_menyita'),
            'penyidik_nama' => $request->input('penyidik_nama'),
            'penyidik_pangkat_nrp' => $request->input('penyidik_pangkat'),
            'penyidik_jabatan' => $request->input('penyidik_jabatan'),
            'penyidik_kesatuan' => $request->input('penyidik_kesatuan'),
            'disahkan_di' => $request->input('disahkan_di'),
            'disahkan_tgl' => $request->input('disahkan_tanggal'),
            'disahkan_nama' => $request->input('disahkan_nama'),
            'disahkan_pangkat_nrp' => $request->input('disahkan_pangkat'),
            'disahkan_jabatan' => $request->input('disahkan_jabatan'),
            'disahkan_kesatuan' => $request->input('disahkan_kesatuan'),
            'status' => $status,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $catatan_pelanggaran = DB::table('catatan_pelanggaran')->insertGetId($insert);
        if(@$catatan_pelanggaran) {
            $response = $this->helper->success(['id' => $catatan_pelanggaran], 'Berhasil Membuat Data !');
        } else {
            $response = $this->helper->error('Gagal Membuat Data !', 404);
        }

        return $response;
    }

    public function update($request)
    {
        $id = $request->input('id_pelanggaran');
        $id_detail_pelanggaran = $request->input('id_detail_pelanggaran');
        $id_master_pelanggaran = $request->input('id_master');
        $id_member = $request->input('id_member');

        $tgl = $request->input('tgl');
        $newDate = date("Y-m-d", strtotime($tgl));

        $updateDetail = [];
        $update = [];
        if(!empty($id_member)) {
            $update['id_member'] = $id_member;
        }

        if(!empty($newDate)) {
            $update['tgl'] = $newDate;
        }

        if(!empty($id_detail_pelanggaran)) {
            $updateDetail['id'] = $id_detail_pelanggaran;
        }

        if(!empty($id_master_pelanggaran)) {
            $updateDetail['id_master_pelanggaran'] = (string)json_encode($id_master_pelanggaran);
        } else {
            $updateDetail['id_master_pelanggaran'] = (string)json_encode(null);
        }

        if(!empty($id_master_pelanggaran)) {
            $update['status'] = 'Melanggar';
        } else {
            $update['status'] = 'Lengkap';
        }

        if(!empty($id)) {
            $updateDetail['id_catatan_pelanggaran'] = $id;
        }

        if($id) {
            $detail_pelanggaran = $this->detail->update($updateDetail);
            $update['updated_at'] = Carbon::now();
            $catatan_pelanggaran = DB::table('catatan_pelanggaran')->where('id', (int)$id)->update($update);
            if(@$detail_pelanggaran) {
                if(@$catatan_pelanggaran) {
                    $update['id'] = $id;
                    $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');
                } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
            } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

    public function delete($id)
    {
        if ($id) {
            $detail_pelanggaran = DB::table('detail_pelanggaran')->delete($id);
            $catatan_pelanggaran = DB::table('catatan_pelanggaran')->delete($id);
            if (@$catatan_pelanggaran && @$detail_pelanggaran) $response = $this->helper->success(['id' => $id, 'id_detail' => $id], 'Berhasil Menghapus Data Dengan ID '.$id.'!');
            else $response = $this->helper->error('Gagal Menghapus Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

}
