<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Helpers\Helper;
use Illuminate\Support\Carbon;

class DetailPelanggaranController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function getOne($id)
    {
        $detail_pelanggaran = DB::table('detail_pelanggaran')->where('id', $id)->get()->toArray();
        switch (true) {
            case $detail_pelanggaran == null|| $detail_pelanggaran == '' || empty($detail_pelanggaran) :
                return $this->helper->error('Data Tidak Ditemukan!', 404);
                break;
        }
        return $this->helper->success($detail_pelanggaran, 'Data Ditemukan!');
    }

    public function add($request)
    {
        $id_master_pelanggaran = $request['id_master_pelanggaran'];
        $id_catatan_pelanggaran = $request['id_catatan_pelanggaran'];

        $insert = [
            'id_master_pelanggaran' => $id_master_pelanggaran,
            'id_catatan_pelanggaran' => $id_catatan_pelanggaran,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $detail_pelanggaran = DB::table('detail_pelanggaran')->insertGetId($insert);

        if(@$detail_pelanggaran) {
            $response = $this->helper->success($insert, 'Berhasil Membuat Data !');
        } else {
            $response = $this->helper->error('Gagal Membuat Data !', 404);
        }

        return $response;
    }

    public function update($request)
    {
        $id = $request['id'];
        $id_master_pelanggaran = $request['id_master_pelanggaran'];
        $id_catatan_pelanggaran = $request['id_catatan_pelanggaran'];

        $update = [];
        if(!empty($id_master_pelanggaran)) {
            $update['id_master_pelanggaran'] = $id_master_pelanggaran;
        }
        if(!empty($id_catatan_pelanggaran)) {
            $update['id_catatan_pelanggaran'] = $id_catatan_pelanggaran;
        }

        if($id) {
            $update['updated_at'] = Carbon::now();
            $detail_pelanggaran = DB::table('detail_pelanggaran')->where('id', (int)$id)->update($update);
            if(@$detail_pelanggaran) {
                $update['id'] = $id;
                $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');
            } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }

}
