<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Helpers\Helper;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function viewAdminGet(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('admins')->latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('role', function($row){
                        if($row->role === 'Special') {
                            return 'Komandan';
                        }
                        if($row->role === 'Regular') {
                            return 'Member';
                        }
                    })
                    ->addColumn('action', function($row){
                        $btn = "<center><a href='".route('view.admin.detail', $row->id)."' data-toggle='tooltip' data-original-title='Show' class='btn btn-success btn-sm'>Show</a> <a href='".route('view.admin.update', $row->id)."' data-toggle='tooltip' data-original-title='Edit' class='btn btn-primary btn-sm'>Edit</a> <a onclick='deleteConfirm(".$row->id.")' data-toggle='tooltip' data-original-title='Delete' id='deleted_button' class='btn btn-danger btn-sm'>Delete</a> </center>";
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.get');
    }

    public function viewAdminDetail($id)
    {
        $data = $this->getOne($id);
        return view('admin.detail')->with('data', @$data['data']);
    }

    public function viewAdminCreate()
    {
        return view('admin.create');
    }

    public function handleAdminCreate(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email'  => 'required',
            'role'  => 'required', //'Special, Regular'
            'password'  => 'required',
        ]);

        $data = $this->add($request);
        if(@$data['status']) {
            return view('admin.create')->with('data', $data['data'])
                                            ->with('success', $data['message']);
        } else {
            return view('admin.create')->with('data', $data['data'])
                                            ->with('error', $data['message']);
        }
    }

    public function viewAdminUpdate($id)
    {
        $data = $this->getOne($id);
        if(@$data['status']) {
            return view('admin.update')->with('data', @$data['data']);
        } else {
            return view('admin.update')->with('error', @$data['message']);
        }
    }

    public function handleAdminUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);

        if($request->input('old_password')){
            $request->validate([
                'id' => 'required',
                'old_password' => 'required',
                'new_password' => 'required',
            ]);
        }

        $data = $this->update($request);
        if(@$data['status']) {
            return redirect(route('view.admin.update', $data['data']['id']))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.admin.update', $request->input('id')))->with('alertErr', @$data['message']);
        }
    }

    public function handleAdminDelete($id)
    {
        $data = $this->delete($id);
        if(@$data['status']) {
            return redirect(route('view.admin'))->with('alert', @$data['message']);
        } else {
            return redirect(route('view.admin'))->with('alertErr', @$data['message']);
        }
    }

    public function getOne($id)
    {
        $admin = DB::table('admins')->where('id', $id)->get()->toArray();
        switch (true) {
            case $admin == null|| $admin == '' || empty($admin) :
                return $this->helper->error('Data Tidak Ditemukan!', 404);
                break;
        }
        return $this->helper->success($admin, 'Data Ditemukan!');
    }

    public function add($request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $role = $request->input('role');
        $password = $request->input('password');

        $insert = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $check = DB::table('admins')->where('email', $email)->first();

        if(@$check) {
            $response = $this->helper->error('Email Sudah Digunakan !', 404);
        } else {

            if((int)$role === 1) {
                $insert['role'] = 'Special';
            }

            if((int)$role === 2) {
                $insert['role'] = 'Regular';
            }

            $members = DB::table('admins')->insert($insert);

            if($members) {
                $response = $this->helper->success($insert, 'Berhasil Membuat Data !');
            } else {
                $response = $this->helper->error('Gagal Membuat Data !', 404);
            }
        }
        return $response;
    }

    public function update($request)
    {
        $id = $request->input('id');
        $name = $request->input('name');
        $emailLama = $request->input('email_lama');
        $emailBaru = $request->input('email_baru');
        $role = $request->input('role');
        $oldPassword = $request->input('old_password');
        $newPassword = $request->input('new_password');

        if( !empty($newPassword) && !empty($oldPassword) ) {
            $data = DB::table('admins')->where('email', $emailLama)->first();
            if(Hash::check($oldPassword, $data->password)) {

                $update = [];

                if( !empty($name) ) {
                    $update['name'] = $name;
                }

                if( !empty($emailBaru) ) {
                    $update['email'] = $emailBaru;
                }

                if( !empty($role) ) {
                    if((int)$role === 1) {
                        $update['role'] = 'Special';
                    }
                    if((int)$role === 2) {
                        $update['role'] = 'Regular';
                    }
                }
                $update['password'] = Hash::make($newPassword);
                if($id) {
                    $update['updated_at'] = Carbon::now();
                    $members = DB::table('admins')->where('id', (int)$id)->update($update);
                    if(@$members) {
                        $update['id'] = $id;
                        return $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');die();
                    } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
                } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);
            } else {
                $response = $this->helper->error('Password Lama anda Salah !', 400);
            }
        } else {
            if($id) {
                $update['updated_at'] = Carbon::now();
                $members = DB::table('admins')->where('id', (int)$id)->update($update);
                if(@$members) {
                    $update['id'] = $id;
                    return $response = $this->helper->success($update, 'Berhasil Meng-Update Data !');die();
                } else $response = $this->helper->error('Gagal Meng-Update Data !', 404);
            } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);
        }

        return $response;
    }

    public function delete($id)
    {
        if ($id) {
            $members = DB::table('admins')->delete($id);
            if (@$members) $response = $this->helper->success(['id' => $id], 'Berhasil Menghapus Data Dengan ID '.$id.'!');
            else $response = $this->helper->error('Gagal Menghapus Data !', 404);
        } else $response = $this->helper->error('Tolong Masukan ID Data !', 404);

        return $response;
    }
}
