<?php

namespace App\Http\Controllers;

use PDF;

class ExampleController extends Controller
{
    private $catatanPelanggaran;
    private $member;

    public function __construct()
    {
        $this->catatanPelanggaran = new CatatanPelanggaranController;
        $this->member = new MemberController;
    }


    public function index()
    {
        $data = $this->catatanPelanggaran->getOne(2);
        $id_member=0;
        foreach($data['data']['catatan'] as $key => $value) {
            $id_member+=$value->id_member;
        }
        $members = $this->member->getOne($id_member);
        $mainData = [
            'member' => $members['data'],
            'catatan' => $data['data']['catatan'],
            'data_master' => $data['data']['data_master'],
            'id_data_master' => $data['data']['id_data_master']
        ];
        // print_r($data['data']);die();
        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        return PDF::loadView('pdf_new', ['data' => @$mainData])->stream();
        // return view('pdf_new');
    }

}
